package com.iitb.cse.arkenstone.safe.quiz.services;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.scheduling.GcmJobSchedulerService;
import com.iitb.cse.arkenstone.safe.base.SafeApplication;

/**
 * Job service for integrating GCM Network Manager
 * with Priority Job Queue.
 */
public class GcmJobService extends GcmJobSchedulerService {
    @NonNull
    @Override
    protected JobManager getJobManager() {
        return SafeApplication.getJobManager();
    }
}
