package com.iitb.cse.arkenstone.safe.utils;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.iitb.cse.arkenstone.safe.R;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Utility class for functions which require a context.
 */
public final class ContextHelper {

    /**
     * The Singleton instance.
     */
    private static final ContextHelper instance = new ContextHelper();

    /**
     * Context of the application.
     */
    private static Context context = null;

    /**
     * Private constructor to defeat instantiation.
     */
    private ContextHelper() {
        // No body required
    }

    /**
     * Create instance method to attach the context.
     *
     * @param application The app to attach the context.
     */
    public static void createInstance(Context application) {
        context = application;
    }

    /**
     * Function to check whether current device is tablet or phone.
     *
     * @return true if current device is tablet.
     */
    public static boolean isTablet() {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Set hide keyboard when user touches anywhere else in screen.
     *
     * @param view     The view to enable hiding
     * @param activity Activity in which the view exists
     */
    public static void setupHideKeyboardOnOutsideTouch(
            final View view, final Activity activity) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(final View view,
                                       final MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                //Recursive call to children
                setupHideKeyboardOnOutsideTouch(innerView, activity);
            }
        }
    }

    /**
     * Hide the keyboard from calling activity.
     *
     * @param activity The activity from which keyboard should
     *                 be hidden
     */
    public static void hideSoftKeyboard(final Activity activity) {
        if (activity == null) {
            return;
        }
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);

        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


    /**
     * Shows default instructions defined in the string resource
     * using an alert dialog. When user checks never show again,
     * updates the show instructions preference
     *
     * @param activity reference to caller activity;
     */
    public static void showInstructions(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // Get the layout inflater
        LayoutInflater inflater = activity.getLayoutInflater();

        // cannot pass null as parameter to inflate - lint error
        // Instead pass root of given activity and specify not
        // to attach to this root by settings false as last parameter
        ViewGroup viewGroup = (ViewGroup)
                activity.getWindow().getDecorView().getRootView();
        View view = inflater.inflate(
                R.layout.instruction_dialog, viewGroup, false);

        // when never show again checkbox is
        // clicked need to update the preference
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.instruction_skip);
        checkBox.setOnCheckedChangeListener(new CompoundButton
                .OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView,
                                         final boolean isChecked) {
                updateShowInstructionsPreference(isChecked, activity);
            }
        });

        // Inflate and set the layout for the dialog
        builder.setTitle("General Instructions")
                .setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        // once instructions are read check for permissions.
                        PermissionUtils.checkAndRequestPermission(activity);
                    }
                })
                .setCancelable(false);

        builder.show();
    }

    /**
     * This will disable the show instructions preference
     * when user checked never show again.
     *
     * @param isChecked whether user checked or not
     * @param activity  reference to caller activity
     */
    static void updateShowInstructionsPreference(final boolean isChecked,
                                                 final Activity activity) {
        SharedPreferences sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(activity);

        // If is not checked then show instruction again
        sharedPrefs.edit().putBoolean(
                AppConstant.PREF_SHOW_INSTRUCTIONS, !isChecked).apply();
    }

    /**
     * Get the list of floating applications.
     *
     * @return true if there is some app false otherwise
     */
    public static boolean logFloatingApps() {

        ArrayList<String> floatingAppsNames = new ArrayList<>();
        ArrayList<String> floatingAppsRunning = new ArrayList<>();
        ArrayList<String> floatingAppsRunningForeground = new ArrayList<>();

        ArrayList<String> floatingAppsServicesRunning = new ArrayList<>();
        ArrayList<String> floatingAppsServicesRunningForeground =
                new ArrayList<>();


        PackageManager packageManager = context.getPackageManager();
        ActivityManager activityManager = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo>
                runningAppProcesses = activityManager.getRunningAppProcesses();

        List<PackageInfo> appList = packageManager.getInstalledPackages(0);
        List<PackageInfo> floatingApps = new ArrayList<>();

        // we can once collect all floating app and can
        // check whether their services are running
        // without doing the for loop every time
        for (PackageInfo app : appList) {
            if (app.packageName.equals(context.getPackageName())) {
                // we should exclude our application from list
                continue;
            }

            if (PackageManager.PERMISSION_GRANTED != packageManager
                    .checkPermission(Manifest.permission.SYSTEM_ALERT_WINDOW, app.packageName)) {
                continue;
            }

            floatingApps.add(app);
            floatingAppsNames.add((String) app.applicationInfo.loadLabel(packageManager));

            for (int i = 0; i < runningAppProcesses.size(); i++) {
                if (runningAppProcesses.get(i).processName.equals(app.packageName)) {

                    floatingAppsRunning.add(" " + app
                            .applicationInfo.loadLabel(packageManager));

                    if (runningAppProcesses.get(i).lru
                            == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {

                        floatingAppsRunningForeground.add(
                                " " + app.applicationInfo.loadLabel(packageManager));
                    }
                }
            }

        }

        if (floatingAppsNames.isEmpty()) {
            return false;
        }

        for (PackageInfo floatingApp : floatingApps) {
            for (ActivityManager.RunningServiceInfo service
                    : activityManager.getRunningServices(Integer.MAX_VALUE)) {

                if (service.service.getPackageName()
                        .equals(floatingApp.packageName)) {

                    floatingAppsServicesRunning.add(" "
                            + floatingApp.applicationInfo.loadLabel(packageManager));

                    if (service.foreground) {
                        logDebug(logTag(),
                                "Service Foreground",
                                "last activity time"
                                        + service.lastActivityTime + " "
                                        + service.started);

                        floatingAppsServicesRunningForeground.add(" " + floatingApp
                                .applicationInfo
                                .loadLabel(packageManager));
                    }
                }
            }
        }

        logDebug(logTag(), "list of floating apps installed "
                + floatingAppsNames.toString());
        logDebug(logTag(), "list of floating apps running  "
                + floatingAppsRunning.toString());
        logDebug(logTag(), "list of floating apps foreground "
                + floatingAppsRunningForeground.toString());
        logDebug(logTag(), "list of floating app services running "
                + floatingAppsServicesRunning.toString());
        logDebug(logTag(), "list of floating app services running foreground "
                + floatingAppsServicesRunningForeground.toString());

        // TODO.. Complete this
        return !floatingAppsServicesRunningForeground.isEmpty();
    }

    /**
     * Detect whether we are running a emulator or not.
     *
     * @return True if emulator.
     */
    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic")
                || "google_sdk".equals(Build.PRODUCT);
    }

    /**
     * Convert date string into java date format.
     *
     * @param dateString The string in ISO form.
     * @return Java date object
     */
    public static Date formatDate(String dateString) {
        DateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateFormat format2 = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);

        Date date;

        if (dateString == null) {
            return null;
        }

        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            try {
                date = format2.parse(dateString);
            } catch (ParseException e1) {
                date = null;
            }
        }
        return date;
    }

    /**
     * Convert IP address to string
     */
    public static String convertIPtoString(int ipAddress) {

        int ip = ipAddress;
        // Convert little-endian to big-endian if needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ip = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ip).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            logDebug(logTag(), "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }

    /**
     * For checking if screen is off when focus is lost
     * This can be because phone go auto locked after some time
     */
    public static boolean isScreenOn() {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = powerManager.isScreenOn();
        logDebug(logTag(), "QuizApp", "Screen on: " + isScreenOn);
        return isScreenOn;
    }
}
