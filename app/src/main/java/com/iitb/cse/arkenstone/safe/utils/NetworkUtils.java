package com.iitb.cse.arkenstone.safe.utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.iitb.cse.arkenstone.safe.quiz.QuizLogging;

/**
 * Singleton for Network based utilities.
 */
public final class NetworkUtils {

    /**
     * Enum to denote a network mode.
     */
    public enum ActiveNetwork {
        /**
         * Wifi network.
         */
        WIFI,

        /**
         * Mobile data.
         */
        MOBILE,

        /**
         * Others and no network.
         */
        OTHER,

        /**
         * Bluetooth.
         */
        BLUETOOTH
    }

    /**
     * Instance for singleton class.
     */
    private static NetworkUtils instance = null;

    /**
     * App context required for the utility class.
     */
    private static Context context = null;

    /**
     * Info about the currently active network.
     */
    private static NetworkInfo currentActiveNetworkInfo;

    /**
     * Connectivity Manager taken from context.
     */
    private static ConnectivityManager cm;

    /**
     * Private constructor to defeat instantiation.
     */
    private NetworkUtils() {
        // Body not required
    }

    /**
     * Create instance method for singleton.
     *
     * @param app Application for getting context.
     */
    public static void createInstance(final Context app) {
        instance = new NetworkUtils();
        context = app;
        cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        currentActiveNetworkInfo = cm.getActiveNetworkInfo();
    }

    /**
     * Just to check if network is available or not.
     * This is to be further improved to see if the server is reachable or not
     *
     * @return true if available, else false.
     */
    public static boolean isNetworkAvailable() {

        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        // if no network is available activeNetworkInfo will be null
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnected();
    }

    /**
     * This function cannot detect whether mobile data is enabled or not.
     * It detects whether current active network is mobile network or not
     *
     * @return true if current active network is mobile network
     */
    public static boolean isMobileDataActive() {
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnected()
                && activeNetworkInfo.getType()
                == ConnectivityManager.TYPE_MOBILE;
    }

    /**
     * Detect whether bluetooth is on or off.
     *
     * @return True if bluetooth is on, false otherwise
     */
    public static boolean isBlueToothOn() {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        return btAdapter != null && btAdapter.isEnabled();
    }

    /**
     * This function reports wifi ssid, bssid, ip address, mac and imei numbers to server.
     */
    public static void reportWifissid() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (wifiManager == null) return;

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            String ssid = wifiInfo.getSSID();
            if (ssid.charAt(0) != '"') {
                ssid = "\"" + ssid + "\"";
            }

            String bssid = wifiInfo.getBSSID();
            if (bssid.charAt(0) != '"') {
                bssid = "\"" + bssid + "\"";
            }
            String ipAddr = ContextHelper.convertIPtoString(wifiInfo.getIpAddress());
            String mac = wifiInfo.getMacAddress();
            String imei = tm.getDeviceId();

            // We are just ignoring the reply
            QuizLogging.newLog(new QuizLogging.QuizLog(
                    "{\"Wifi SSID\": " + ssid + ", \"Wifi BSSID\": " + bssid
                            + ", \"IP Addr\": \"" + ipAddr
                            + "\", \"Mac\": \"" + mac + "\", \"IMEI\" : \"" + imei + "\"}",
                    QuizLogging.QuizLog.Severity.INFO));
        }
    }

    /**
     * Ensure bluetooth and mobile data are turned off.
     *
     * @param activity The activity that called this. Required to show taast
     *                 errors messages.
     * @param log      Should log this to the instructor or not.
     * @return True if there everything is off. False otherwise.
     */
    public static boolean externalConnectionOff(final Activity activity,
                                                final boolean log) {
        boolean ok = true;

        String msg = "Please turn off ";
        if (isBlueToothOn()) {
            msg += "Bluetooth ";
            ok = false;
        }
        if (isMobileDataActive()) {
            if (!ok) {
                msg += "and ";
            }
            msg += "Mobile Data ";
            ok = false;
        }

        msg += ".It will be notified to the instructor.";

        if (!ok) {
            final String finalMsg = msg;
            if (log) {
                // TODO this : done by Sonika
                QuizLogging.newLog(new QuizLogging.QuizLog(
                        "Mobile data or bluetooth detected!",
                        QuizLogging.QuizLog.Severity.CRITICAL));
            }
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity,
                            finalMsg, Toast.LENGTH_LONG).show();
                }
            });
        }
        return ok;
    }

    /**
     * Get the current active network info.
     *
     * @return The NetworkInfo object.
     */
    public static NetworkInfo getCurrentActiveNetworkInfo() {
        return currentActiveNetworkInfo;
    }

    /**
     * Set the current active network info.
     *
     * @param activeNetworkInfo The object which it should be set to.
     */
    public static void setCurrentActiveNetworkInfo(
            final NetworkInfo activeNetworkInfo) {
        NetworkUtils.currentActiveNetworkInfo = activeNetworkInfo;
    }

    /**
     * This function based on network type add connectivity change log
     * to QuizLogging. Currently all log levels are red except for wifi.
     * Can be changed if needed
     *
     * @param msg     message that log must contain
     * @param network what type of network is currently connected to
     */
    public static void logConnectivityChange(
            final String msg,
            final ActiveNetwork network) {
        // check quiz has started otherwise we cannot send logs
//        if (!AppData.isQuizStarted()) {
//            return;
//        }

        // TODO : quizlog written by Sonika
        if (network == ActiveNetwork.WIFI) {
            // no need to send log that wifi is on because if wifi is connected to valid router
            // then it is unnecessary to show this log
            // anyhow reportWifissid raises red flag when connected wifi router's ssid doesn't match
            // with intended router
            reportWifissid();
        } else if (network == ActiveNetwork.MOBILE) {
            QuizLogging.newLog(new QuizLogging.QuizLog(msg, QuizLogging.QuizLog.Severity.CRITICAL));
        } else if (network == ActiveNetwork.OTHER) {
            QuizLogging.newLog(new QuizLogging.QuizLog(msg, QuizLogging.QuizLog.Severity.CRITICAL));
        } else if (network == ActiveNetwork.BLUETOOTH) {
            QuizLogging.newLog(new QuizLogging.QuizLog(msg, QuizLogging.QuizLog.Severity.CRITICAL));
        }
    }
}
