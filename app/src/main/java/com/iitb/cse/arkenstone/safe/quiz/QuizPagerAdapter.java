package com.iitb.cse.arkenstone.safe.quiz;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.iitb.cse.arkenstone.safe.quiz.questions.data.Module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The class used to create the adapter for
 * pages for different questions.
 */
public class QuizPagerAdapter extends FragmentStatePagerAdapter {

    /**
     * Store all the modules.
     */
    public ArrayList<Module> moduleList;

    /**
     * Maintain a map from the question number to the
     * module. This is used for faster {@link #getItem(int)}
     */
    public Map<Integer, Module> moduleCountMap = new HashMap<>();

    /**
     * Store the number of questions for faster access.
     */
    Integer numberOfQuestions;

    Map<Integer, Fragment> itemMap = new HashMap<>();

    /**
     * Create pager adapter with given questions.
     *
     * @param fm      Fragment Manager for the adapter.
     * @param modules The list of modules for the quiz.
     */
    public QuizPagerAdapter(FragmentManager fm, ArrayList<Module> modules) {
        super(fm);
        moduleList = modules;
        numberOfQuestions = 0;

        for (Module module : moduleList) {
            for (int i = numberOfQuestions;
                 i < numberOfQuestions + module.questions.size();
                 i++) {
                moduleCountMap.put(i, modules.get(i));
            }
            numberOfQuestions += module.questions.size();
        }
    }

    @Override
    public Fragment getItem(int position) {
        if (!itemMap.containsKey(position)) {
            itemMap.put(position, moduleCountMap.get(position).getQuestionView(position + 1));
        }
        return itemMap.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        itemMap.remove(position);
    }

    @Override
    public int getCount() {
        return numberOfQuestions;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return moduleCountMap.get(position).getQuestion(position + 1).questionNumber;
    }
}
