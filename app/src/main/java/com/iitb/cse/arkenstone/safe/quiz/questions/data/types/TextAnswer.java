package com.iitb.cse.arkenstone.safe.quiz.questions.data.types;

import com.google.gson.annotations.SerializedName;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import java.util.ArrayList;

/**
 * Class for question with text as answer.
 */
public class TextAnswer extends Question {

    /**
     * Submitted response for the question
     * in case of PreviousQuiz.
     */
    @SerializedName("submitted_response")
    public String submittedResponse;

    /**
     * List of answers in a question.
     * in case of PreviousQuiz
     */
    @SerializedName("answers")
    public ArrayList<String> answers = null;

    public TextAnswer() {
        this.type = AppConstant.TYPE_TEXT_ANSWER;
    }

    @Override
    public void post_save() {
    }
}
