package com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.github.clans.fab.FloatingActionButton;
import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.QuizFab;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.McqMultipleAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.QuestionFragment;
import com.iitb.cse.arkenstone.safe.utils.AppData;

import java.util.ArrayList;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

/**
 * Fragment for Multiple Choice question with
 * multiple answers.
 */
public class McqMultipleAnswerFrag extends QuestionFragment
        implements CompoundButton.OnCheckedChangeListener {

    /**
     * The question object for accessing its details.
     */
    McqMultipleAnswer fullQuestion;

    /**
     * The buttons for options.
     */
    ArrayList<CheckBox> buttons = new ArrayList<>();

    @Override
    public void generateChildView(View inflatedLayout) {
        fullQuestion = (McqMultipleAnswer) question;

        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        for (String option : fullQuestion.shuffledOptions) {
            LinearLayout layoutForQuestion = new LinearLayout(getContext());
            layoutForQuestion.setOrientation(LinearLayout.HORIZONTAL);
            layoutForQuestion.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            final CheckBox button = new CheckBox(getContext());
            button.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            button.setId(View.generateViewId());
            button.setTag(fullQuestion.options.indexOf(option));

            // Bold and green correct answers if only one set of correct answers present
            if (fullQuestion.answers != null) {
                if (fullQuestion.answers.size() == 1) {
                    for (Integer answer : fullQuestion.answers.get(0)) {
                        if (option.equals(fullQuestion.options.get(answer))) {
                            option = "<span style=\"color:green\"> <b>" + option + "</b></span>";
                        }
                    }
                }
            }

            button.setOnCheckedChangeListener(this);

            final WebView optionWebView = AppData.getWebViewCache()
                    .get("<span hidden>" + question.id + "</span>" + option);
            optionWebView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            optionWebView.setPadding(0, 0, 15, 0);
            optionWebView.setId(View.generateViewId());
            optionWebView.setTag(button);
            optionWebView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    view.performClick();
                    if (view.getId() == optionWebView.getId()
                            && motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        ((Button) view.getTag()).performClick();
                    }
                    return false;
                }
            });

            // Remove old parents (may be called when reusing)
            if (optionWebView.getParent() != null) {
                ((ViewGroup) optionWebView.getParent()).removeView(optionWebView);
            }

            //For PreviousQuiz. Disable button and webView if submitted_response available
            if (fullQuestion.submittedResponse != null) {
                button.setEnabled(false);
                optionWebView.setEnabled(false);
            }

            buttons.add(button);
            layoutForQuestion.addView(button);

            layoutForQuestion.addView(optionWebView);
            layout.addView(layoutForQuestion);
        }

        if (fullQuestion.answers != null) {
            if (fullQuestion.answers.size() > 1) {
                String answerString = "<span style=\"color:green\"> <b><center>ANSWER:</center>";
                for (int set = 0; set < fullQuestion.answers.size(); set++) {
                    for (int i = 0; i < fullQuestion.answers.get(set).size(); i ++) {
                        int answer = fullQuestion.answers.get(set).get(i);
                        String option = fullQuestion.options.get(answer);
                        answerString += (char)((int) 'A' + fullQuestion.options.indexOf(option));
                        if (i != fullQuestion.answers.get(set).size() - 1
                                && fullQuestion.answers.get(set).size() > 1) {
                            answerString += ", ";
                        }
                    }
                    if (set != fullQuestion.answers.size() - 1) {
                        answerString += "\n <center> OR </center>\n";
                    } else {
                        answerString += "</b></span>";
                    }
                }
                logDebug(logTag(), answerString);
                final WebView answerWebView = AppData.getWebViewCache()
                        .get("<span hidden>" + question.id + "</span>" + answerString);
                answerWebView.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                answerWebView.setEnabled(false);
                layout.addView(answerWebView);
            }
        }

        LinearLayout layoutDisplay = (LinearLayout) inflatedLayout.findViewById(R.id.frag_question);
        layoutDisplay.addView(layout);
        // If response is already available, select it.
        if (!"".equals(question.getResponse())) {
            for (Integer selectedOption : parseResponseString(question.getResponse())) {
                buttons.get(fullQuestion
                        .shuffledOptions
                        .indexOf(fullQuestion.options.get(selectedOption))).performClick();
            }
        }

        //For PreviousQuiz. Click submitted response if available.
        if (fullQuestion.submittedResponse != null) {
            for (Integer selectedOption : fullQuestion.submittedResponse) {
                buttons.get(fullQuestion
                        .shuffledOptions
                        .indexOf(fullQuestion.options.get(selectedOption))).performClick();
            }
        }
    }

    @Override
    public void setupFab() {
        QuizFab.instance.resetFab();

        if (question.reasonTextLength != 0) {
            //TODO fab for reason text
            QuizFab.instance.addButton(QuizFab.instance.getClearReasonBtn());
        }

        final FloatingActionButton clearOtherBtn = QuizFab.instance.getClearOtherBtn();
        clearOtherBtn.setLabelText("Clear Answers");
        clearOtherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (CheckBox checkBox : buttons) {
                    checkBox.setChecked(false);
                }
                QuizFab.instance.collapseFab();
            }
        });
        QuizFab.instance.addButton(clearOtherBtn);
    }

    @Override
    public void freezeQuestion() {
        for (CheckBox button : buttons) {
            button.setEnabled(false);
        }
        QuizFab.instance.hideFab();
    }

    ArrayList<Integer> parseResponseString(String responseString) {
        String[] split = responseString.split(" ");
        ArrayList<Integer> responses = new ArrayList<>();
        for (String val : split) {
            responses.add(Integer.parseInt(val));
        }
        return responses;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        // Disable check if PreviousQuiz
        if (fullQuestion.submittedResponse == null) {
            Integer optionIndex = (Integer) compoundButton.getTag();
            String responseString = question.getResponse();


            if (checked) {
                logDebug(logTag(), "Checked option index is " + optionIndex);
                if (!responseString.contains(String.valueOf(optionIndex))) {
                    responseString = responseString + " " + optionIndex;
                }
            } else {
                logDebug(logTag(), "Unchecked option index is " + optionIndex);
                responseString = responseString.replace(String.valueOf(optionIndex), "");
            }

            question.setResponse(responseString);
        }
    }
}
