package com.iitb.cse.arkenstone.safe;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;

/**
 * Settings activity screen.
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {

        if (ContextHelper.isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        super.onCreate(savedInstanceState);

        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    /**
     * Fragment for settings screen.
     */
    public static class SettingsFragment extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public final void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.settings);

            // If you want to enable setting to
            // show instruction remove below line.
            getPreferenceScreen()
                    .removePreference(getPreferenceScreen()
                    .findPreference(AppConstant.PREF_SHOW_INSTRUCTIONS));
        }

        @Override
        public final void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public final void onSharedPreferenceChanged(
                final SharedPreferences sharedPreferences,
                final String key) {

            // Add a trailing slash for the url
            if (key.equals(AppConstant.PREF_SERVER_URL)) {
                String value = sharedPreferences.getString(key, AppConstant.DEFAULT_BASE_URL);
                if (!value.endsWith("/")) {
                    value += "/";
                }
                sharedPreferences.edit().putString(key, value).apply();
            }

            ServerInterface.reloadServerBaseUrl(getActivity());
            LoginActivity.reloadOrgList();
        }

        @Override
        public final void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }
    }
}
