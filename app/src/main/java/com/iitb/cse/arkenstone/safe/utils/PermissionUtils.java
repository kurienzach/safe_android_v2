package com.iitb.cse.arkenstone.safe.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.iitb.cse.arkenstone.safe.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility singleton for permissions.
 */
public final class PermissionUtils {

    /**
     * Instance for singleton.
     */
    private static PermissionUtils instance = new PermissionUtils();

    /**
     * Private method to defeat instantiation.
     */
    private PermissionUtils() {
        // Body not needed
    }

    /**
     * This function checks and request any pending permissions.
     *
     * @param activity reference to activity which has called this function
     */
    public static void checkAndRequestPermission(final Activity activity) {
        List<String> permissionList = new ArrayList<>();

        addPermission(
                permissionList,
                Manifest.permission.READ_PHONE_STATE,
                activity);

        addPermission(
                permissionList,
                Manifest.permission.CAMERA,
                activity);

        // checking all required permissions are already granted or not
        if (permissionList.isEmpty()) {
            // all permissions are granted
            // need to ask for system level window permission
            handleSystemOverlayPermission(activity);

        } else {
            // request pending permissions
            String[] permissions = permissionList.toArray(
                    new String[permissionList.size()]);
            ActivityCompat.requestPermissions(
                    activity,
                    permissions,
                    AppConstant.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        }
    }

    /**
     * Add the permission which is denied to the list.
     *
     * @param permissionList List of permissions that need to be requested
     * @param permission     Which permission
     * @param activity       activity reference
     */
    private static void addPermission(final List<String> permissionList,
                                      final String permission,
                                      final Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED) {

            permissionList.add(permission);
        }

    }

    /**
     * This function checks current build version and
     * accordingly requests for system overlay permission.
     * Shows a dialog explaining why the app needs permission
     * When user clicks ok, it will take user to app settings.
     *
     * @param activity reference to activity
     */
    public static void handleSystemOverlayPermission(final Activity activity) {

        // system overlay permission is explicitly
        // required for only Android M and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(activity)) {

            String message = activity.getResources()
                    .getString(R.string.permission_system_overlay_message);

            DialogInterface.OnClickListener listener = new DialogInterface
                    .OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog,
                                    final int which) {

                    Intent intent = null;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        intent = new Intent(
                                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:"
                                        + activity.getPackageName()));
                    }

                    activity.startActivityForResult(intent,
                            AppConstant.REQUEST_CODE_OVERLAY_PERMISSION);
                }
            };

            new AlertDialog.Builder(activity)
                    .setMessage(message)
                    .setPositiveButton("OK", listener)
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    /**
     * This function is called when user denied permission
     * without checking never ask again. Shows a dialog
     * explaining why the app need permission.
     * When user clicks ok, it requests permission again.
     *
     * @param activity reference to caller activity
     */
    public static void handlePermissionDenied(final Activity activity) {
        // we need to show a dialog explaining why he need to accept permissions
        // when he clicks ok we need to ask permission again

        String message = activity.getResources().getString(
                R.string.permission_rationale_message);

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog,
                                final int which) {
                checkAndRequestPermission(activity); // ask permission again
            }
        };

        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("Ok", onClickListener)
                .setCancelable(false)
                .create()
                .show();
    }

    /**
     * This function is called when user denied permission
     * with checking never ask again. Shows a dialog explaining
     * why the app need permission. When user clicks ok,
     * we need to take user to app settings.
     *
     * @param activity reference to caller activity
     */
    public static void handleNeverShowAgain(final Activity activity) {
        // we need to show a dialog explaining why he need to accept permission
        // when he clicks ok we need to take user to app settings

        String message = activity.getResources()
                .getString(R.string.permission_never_ask_again_rationale);
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog,
                                final int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                intent.setData(uri);
                activity.startActivityForResult(intent, AppConstant.REQUEST_PERMISSION_SETTING);
            }
        };

        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("Ok", onClickListener)
                .setCancelable(false)
                .create()
                .show();
    }
}
