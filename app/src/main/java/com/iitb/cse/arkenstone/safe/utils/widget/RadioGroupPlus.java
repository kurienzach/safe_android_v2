package com.iitb.cse.arkenstone.safe.utils.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IdRes;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * This file is 'nearly' a fork of the default RadioGroup with
 * adjustments to include non Radio Button children.
 *
 * @see https://github.com/worker8/RadioGroupPlus
 */


/**
 * <p>This class is used to create a multiple-exclusion scope for a set of radio
 * buttons. Checking one radio button that belongs to a radio group unchecks
 * any previously checked radio button within the same group.</p>
 * <p></p>
 * <p>Initially, all of the radio buttons are unchecked. While it is not possible
 * to uncheck a particular radio button, the radio group can be cleared to
 * remove the checked state.</p>
 * <p></p>
 * <p>The selection is identified by the unique id of the radio button as defined
 * in the XML layout file.</p>
 *
 * @see RadioButton
 * @see RadioGroup
 * @see LinearLayout
 * @see ViewGroup
 * @see View
 * @see android.widget.LinearLayout.LayoutParams
 */
@SuppressWarnings("GodClass")
public class RadioGroupPlus extends LinearLayout {
    // holds the checked id; the selection is empty by default
    private int checkedId = -1;
    // tracks children radio buttons checked state
    private CompoundButton.OnCheckedChangeListener childOnCheckedChangeListener;
    // when true, onCheckedChangeListener discards events
    private boolean protectFromCheckedChange = false;
    private OnCheckedChangeListener onCheckedChangeListener;
    @SuppressWarnings("FieldCanBeLocal")
    private PassThroughHierarchyChangeListener passThroughListener;

    /**
     * Create a radio group plus which can take radio buttons
     * anywhere in its children.
     */
    public RadioGroupPlus(Context context) {
        super(context);
        setOrientation(VERTICAL);
        init();
    }

    /**
     * Create a radio group plus which can take radio buttons
     * anywhere in its children.
     */
    public RadioGroupPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        childOnCheckedChangeListener = new CheckedStateTracker();
        passThroughListener = new PassThroughHierarchyChangeListener();
        super.setOnHierarchyChangeListener(passThroughListener);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void setOnHierarchyChangeListener(OnHierarchyChangeListener listener) {
        // the user listener is delegated to our pass-through listener
        passThroughListener.onHierarchyChangeListener = listener;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // checks the appropriate radio button as requested in the XML file
        if (checkedId != -1) {
            protectFromCheckedChange = true;
            setCheckedStateForView(checkedId, true);
            protectFromCheckedChange = false;
            setCheckedId(checkedId);
        }
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {

        if (!(child instanceof RadioButton)) {
            super.addView(child, index, params);
            return;
        }

        final RadioButton button = (RadioButton) child;
        if (button.isChecked()) {
            protectFromCheckedChange = true;
            if (checkedId != -1) {
                setCheckedStateForView(checkedId, false);
            }
            protectFromCheckedChange = false;
            setCheckedId(button.getId());
        }

        super.addView(child, index, params);
    }

    /**
     * <p>Sets the selection to the radio button whose identifier is passed in
     * parameter. Using -1 as the selection identifier clears the selection;
     * such an operation is equivalent to invoking {@link #clearCheck()}.</p>
     *
     * @param id the unique id of the radio button to select in this group
     * @see #getCheckedRadioButtonId()
     * @see #clearCheck()
     */
    public void check(@IdRes int id) {
        // don't even bother
        if (id != -1 && id == checkedId) {
            return;
        }

        if (checkedId != -1) {
            setCheckedStateForView(checkedId, false);
        }

        if (id != -1) {
            setCheckedStateForView(id, true);
        }

        setCheckedId(id);
    }

    private void setCheckedId(@IdRes int id) {
        checkedId = id;
        if (onCheckedChangeListener != null) {
            onCheckedChangeListener.onCheckedChanged(this, checkedId);
        }
    }

    private void setCheckedStateForView(int viewId, boolean checked) {
        View checkedView = findViewById(viewId);
        if (checkedView instanceof RadioButton) {
            ((RadioButton) checkedView).setChecked(checked);
        }
    }

    /**
     * <p>Returns the identifier of the selected radio button in this group.
     * Upon empty selection, the returned value is -1.</p>
     *
     * @return the unique id of the selected radio button in this group
     * @see #check(int)
     * @see #clearCheck()
     */
    @IdRes
    public int getCheckedRadioButtonId() {
        return checkedId;
    }

    /**
     * <p>Clears the selection. When the selection is cleared, no radio button
     * in this group is selected and {@link #getCheckedRadioButtonId()} returns
     * null.</p>
     *
     * @see #check(int)
     * @see #getCheckedRadioButtonId()
     */
    public void clearCheck() {
        check(-1);
    }

    /**
     * <p>Register a callback to be invoked when the checked radio button
     * changes in this group.</p>
     *
     * @param listener the callback to call on checked state change
     */
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        onCheckedChangeListener = listener;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new RadioGroupPlus.LayoutParams(getContext(), attrs);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams params) {
        return params instanceof RadioGroup.LayoutParams;
    }

    @Override
    protected LinearLayout.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    public CharSequence getAccessibilityClassName() {
        return RadioGroup.class.getName();
    }

    /**
     * <p>This set of layout parameters defaults the width and the height of
     * the children to {@link #WRAP_CONTENT} when they are not specified in the
     * XML file. Otherwise, this class used the value read from the XML file.</p>
     * <p></p>
     */
    public static class LayoutParams extends LinearLayout.LayoutParams {

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(int width, int height, float initWeight) {
            super(width, height, initWeight);
        }

        public LayoutParams(ViewGroup.LayoutParams params) {
            super(params);
        }

        public LayoutParams(MarginLayoutParams source) {
            super(source);
        }

        /**
         * <p>Fixes the child's width to
         * {@link android.view.ViewGroup.LayoutParams#WRAP_CONTENT} and the child's
         * height to  {@link android.view.ViewGroup.LayoutParams#WRAP_CONTENT}
         * when not specified in the XML file.</p>
         *
         * @param attrSet    the styled attributes set
         * @param widthAttr  the width attribute to fetch
         * @param heightAttr the height attribute to fetch
         */
        @Override
        protected void setBaseAttributes(TypedArray attrSet,
                                         int widthAttr, int heightAttr) {

            if (attrSet.hasValue(widthAttr)) {
                width = attrSet.getLayoutDimension(widthAttr, "layout_width");
            } else {
                width = WRAP_CONTENT;
            }

            if (attrSet.hasValue(heightAttr)) {
                height = attrSet.getLayoutDimension(heightAttr, "layout_height");
            } else {
                height = WRAP_CONTENT;
            }
        }
    }

    /**
     * <p>Interface definition for a callback to be invoked when the checked
     * radio button changed in this group.</p>
     */
    public interface OnCheckedChangeListener {
        /**
         * <p>Called when the checked radio button has changed. When the
         * selection is cleared, checkedId is -1.</p>
         *
         * @param group     the group in which the checked radio button has changed
         * @param checkedId the unique identifier of the newly checked radio button
         */
        void onCheckedChanged(RadioGroupPlus group, @IdRes int checkedId);
    }

    private class CheckedStateTracker implements CompoundButton.OnCheckedChangeListener {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // prevents from infinite recursion
            if (protectFromCheckedChange) {
                return;
            }

            protectFromCheckedChange = true;
            if (checkedId != -1) {
                setCheckedStateForView(checkedId, false);
            }
            protectFromCheckedChange = false;

            int id = buttonView.getId();
            setCheckedId(id);
        }
    }

    /**
     * <p>A pass-through listener acts upon the events and dispatches them
     * to another listener. This allows the table layout to set its own internal
     * hierarchy change listener without preventing the user to setup his.</p>
     */
    private class PassThroughHierarchyChangeListener implements
            ViewGroup.OnHierarchyChangeListener {
        private ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener;

        public void traverseTree(View view) {
            if (view instanceof RadioButton) {
                int id = view.getId();
                // generates an id if it's missing
                if (id == View.NO_ID) {
                    id = View.generateViewId();
                    view.setId(id);
                }
                ((RadioButton) view).setOnCheckedChangeListener(
                        childOnCheckedChangeListener);
            }
            if (!(view instanceof ViewGroup)) {
                return;
            }
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup.getChildCount() == 0) {
                return;
            }
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                traverseTree(viewGroup.getChildAt(i));
            }
        }

        /**
         * {@inheritDoc}.
         */
        public void onChildViewAdded(View parent, View child) {
            traverseTree(child);
            if (parent == RadioGroupPlus.this && child instanceof RadioButton) {
                int id = child.getId();
                // generates an id if it's missing
                if (id == View.NO_ID) {
                    id = View.generateViewId();
                    child.setId(id);
                }
                ((RadioButton) child).setOnCheckedChangeListener(
                        childOnCheckedChangeListener);
            }

            if (onHierarchyChangeListener != null) {
                onHierarchyChangeListener.onChildViewAdded(parent, child);
            }
        }

        /**
         * {@inheritDoc}.
         */
        public void onChildViewRemoved(View parent, View child) {
            if (parent == RadioGroupPlus.this && child instanceof RadioButton) {
                ((RadioButton) child).setOnCheckedChangeListener(null);
            }

            if (onHierarchyChangeListener != null) {
                onHierarchyChangeListener.onChildViewRemoved(parent, child);
            }
        }
    }
}
