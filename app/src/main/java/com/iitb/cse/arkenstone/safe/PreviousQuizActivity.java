package com.iitb.cse.arkenstone.safe;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.iitb.cse.arkenstone.safe.network.NetworkAsyncOperation;
import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.quiz.QuizData;
import com.iitb.cse.arkenstone.safe.quiz.QuizPagerAdapter;
import com.iitb.cse.arkenstone.safe.quiz.questions.QuizDeserializer;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Module;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.TimeUtil;
import com.iitb.cse.arkenstone.safe.utils.widget.WebViewCache;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public class PreviousQuizActivity extends AppCompatActivity implements NetworkAsyncOperation {

    /**
     * List of all question modules for the current quiz.
     */
    public static ArrayList<Module> questionModules;
    /**
     * An object used for maintaining whether quiz
     * interface loading has started or not.
     */
//    private static Boolean quizInterfaceStarted = false;
    /**
     * Time util for Network Async Operation.
     */
    private TimeUtil timeUtil;
    /**
     * Progress bar for the quiz load activity.
     */
    private ProgressBar previousQuizLoadProgress;
    /**
     * Change the behaviour of back button as deemed necessary.
     */
    private volatile Boolean canClickBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        if (ContextHelper.isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_quiz);

        previousQuizLoadProgress = (ProgressBar) findViewById(R.id.previous_quiz_load_progress);
        previousQuizLoadProgress.setIndeterminate(true);

        canClickBackButton = true;
        showProgress(true);

        String quizId = getIntent().getStringExtra("uuid");
        String url = AppConstant.getBaseUrl() + AppConstant.QUIZ_SUBMISSION_DETAIL_URL;
        url = String.format(url, quizId);
        new ServerInterface.NetworkOperation().execute(url, this, null, "AUTH_GET");

        try {
            logDebug(logTag(), "Acquiring webViewCacheLock");
            WebViewCache.webViewCacheLock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            logError(logTag(), "Thread interrupted");
        }

        try {
            logDebug(logTag(), "Creating webViewCache");
            WebViewCache.webViewCacheCreate.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                logDebug(logTag(), "Create webView cache");
                AppData.setWebViewCache(new WebViewCache(PreviousQuizActivity.this));
                logDebug(logTag(), "WebView cache ready");
                WebViewCache.webViewCacheCreate.release();
                WebViewCache.webViewCacheLock.release();
            }
        });

    }

    @Override
    public TimeUtil getTimeUtil() {
        if (timeUtil == null) {
            timeUtil = new TimeUtil();
        }
        return timeUtil;
    }

    private void showProgress(Boolean show) {
        if (show) {
            canClickBackButton = false;
            previousQuizLoadProgress.setVisibility(View.VISIBLE);
        } else {
            canClickBackButton = true;
            previousQuizLoadProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public final void doneNetworkOperation(final String tag,
                                           final String response,
                                           final Integer responseCode) {
        showProgress(false);

        if (responseCode == null) {
            Toast.makeText(this, "Network Error. Try again later", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if (responseCode == 404) {
            Toast.makeText(this, "Unknown quiz. Check the input", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if (responseCode == 401 || responseCode == 403) {
            Toast.makeText(this, "You are not authorized!", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (responseCode != 200) {
            Toast.makeText(this, "Unknown network error!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Extract the data received
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Object>>() {
        }
                .getType();
        Map<String, Object> map = gson.fromJson(response, type);


        //Extracting question modules
        gson = new GsonBuilder()
                .registerTypeAdapter(Question.class, new QuizDeserializer())
                .create();

        Type moduleArrayList = new TypeToken<ArrayList<Module>>() {
        }
                .getType();

        questionModules = gson.fromJson(gson.toJson(map.get("modules")), moduleArrayList);

        scheduleStartQuizInterface();
    }


    /**
     * Try to start the quiz interface and notify if there are any
     * problems that are blocking. This is run on a separate thread which
     * blocks until all the WebViews are ready.
     */
    public void scheduleStartQuizInterface() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    logDebug(logTag(), "Waiting on WebView cache");
                    WebViewCache.webViewCacheCreate.acquire();
                    WebViewCache.webViewCacheLock.acquire(WebViewCache.WebViewCacheSize);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    logDebug(logTag(), "WebView Cache lock fail");
                    return;
                }

                logDebug(logTag(), "WebView Cache load complete");

//                synchronized (this) {
//                    if (quizInterfaceStarted) {
//                        logDebug(logTag(), "quizInterfaceStarted and returned");
//                        return;
//                    } else {
//                        quizInterfaceStarted = true;
//                    }
//                }

                TabLayout tabLayout = (TabLayout) findViewById(R.id.previous_quiz_tab_layout);

                logDebug(logTag(), "Setting up modules and tabs");
                for (int i = 0; i < questionModules.size(); i++) {
                    questionModules.get(i).setModuleNumber(i + 1, tabLayout);
                }
                setupPager(tabLayout);

                WebViewCache.webViewCacheCreate.release();
                WebViewCache.webViewCacheLock.release(WebViewCache.WebViewCacheSize);
                logDebug(logTag(), "Quiz interface ready!");
            }
        }).start();
    }

    private void setupPager(final TabLayout tabLayout) {
        logDebug(logTag(), "Creating the quiz pager");

        final ViewPager quizPager = (ViewPager) findViewById(R.id.previous_quiz_pager);
        final QuizPagerAdapter pagerAdapter = new
                QuizPagerAdapter(getSupportFragmentManager(), questionModules);

        QuizData.setQuizPagerAdapter(pagerAdapter);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                quizPager.setAdapter(pagerAdapter);
                quizPager.setOffscreenPageLimit(4);
                tabLayout.setupWithViewPager(quizPager);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (canClickBackButton) {
            logDebug(logTag(), "Finishing PreviousQuizActivity");
            finish();
            super.onBackPressed();
        }
    }
}
