package com.iitb.cse.arkenstone.safe.quiz.questions;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import android.support.v4.app.Fragment;

import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.FloatAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.IntegerAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.McqMultipleAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.McqSingleAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.TextAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.QuestionFragment;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types.FloatAnswerFrag;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types.IntegerAnswerFrag;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types.McqMultipleAnswerFrag;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types.McqSingleAnswerFrag;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types.TextAnswerFrag;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import java.lang.reflect.Type;
import java.util.HashMap;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

/**
 * Class to deserialize the fetched quiz.
 * Most of the part is using the default Gson deserialization.
 * This class particularly deserializes types of questions
 * into the module class properly following inheritance.
 */
public class QuizDeserializer implements JsonDeserializer<Question> {

    /**
     * Class mapping for mapping question type to data class. We use
     * the double brace initialization method to instantiate here itself.
     */
    static final HashMap<String, Class> classDataMapping = new HashMap<String, Class>() {
        {
            put(AppConstant.TYPE_MCQ_SINGLE_ANSWER, McqSingleAnswer.class);
            put(AppConstant.TYPE_MCQ_MULTIPLE_ANSWER, McqMultipleAnswer.class);
            put(AppConstant.TYPE_FLOAT_ANSWER, FloatAnswer.class);
            put(AppConstant.TYPE_INTEGER_ANSWER, IntegerAnswer.class);
            put(AppConstant.TYPE_TEXT_ANSWER, TextAnswer.class);
        }
    };

    /**
     * Class mapping for mapping question type to fragment class. We use
     * the double brace initialization method to instantiate here itself.
     */
    static final HashMap<String, Class> classFragmentMapping = new HashMap<String, Class>() {
        {
            put(AppConstant.TYPE_MCQ_SINGLE_ANSWER, McqSingleAnswerFrag.class);
            put(AppConstant.TYPE_MCQ_MULTIPLE_ANSWER, McqMultipleAnswerFrag.class);
            put(AppConstant.TYPE_FLOAT_ANSWER, FloatAnswerFrag.class);
            put(AppConstant.TYPE_INTEGER_ANSWER, IntegerAnswerFrag.class);
            put(AppConstant.TYPE_TEXT_ANSWER, TextAnswerFrag.class);
        }
    };

    @Override
    public Question deserialize(
            JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        JsonObject questionObject = json.getAsJsonObject();
        String type = questionObject.get("type").getAsString();

        Class questionClass = classDataMapping.get(type);
        Question instance;

        Gson gson = new Gson();
        instance = (Question) gson.fromJson(json, questionClass);

        return instance;
    }

    /**
     * Return a new instance for the child fragment type for a
     * given question.
     *
     * @param question The question for which fragment is required.
     * @return The fragment for the question.
     */
    public static Fragment getChildFragment(Question question) {
        Class questionFragClass = classFragmentMapping.get(question.type);
        QuestionFragment frag = null;
        try {
            frag = (QuestionFragment) questionFragClass.newInstance();
            frag.setQuestion(question);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return frag;
    }
}
