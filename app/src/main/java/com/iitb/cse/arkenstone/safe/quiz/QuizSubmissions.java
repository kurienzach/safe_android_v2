package com.iitb.cse.arkenstone.safe.quiz;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.iitb.cse.arkenstone.safe.base.SafeApplication;
import com.iitb.cse.arkenstone.safe.network.SafeWebSocket;
import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Module;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.AppData;

import okhttp3.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Class implementing different aspects of quiz submissions
 * including periodic submissions.
 */
public class QuizSubmissions {

    public static final QuizSubmissions instance = new QuizSubmissions();

    /**
     * true if there is a periodic submission waiting to be
     * sent to the server.
     */
    private boolean periodicSubmissionQueued = false;

    /**
     * If we hit {@link QuizSubmissions#tryPeriodicSubmission()} when
     * {@link QuizSubmissions#periodicSubmissionQueued} is true, we
     * retry it after the current one is done.
     */
    private boolean retry = false;

    /**
     * Minimum gap between periodic submissions in seconds.
     */
    public static final Integer PERIODIC_SUBMISSION_INTERVAL = 15;

    /**
     * The old submission data that is used for not sending
     * data when an option is selected and then removed.
     */
    String oldSubmissionData = "";

    private QuizSubmissions() {
        // No body
    }

    /**
     * Get the quiz response GSON serializer..
     *
     * @return The GSON object.
     */
    public static Gson getQuizResponseGson() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers()
                .create();
    }

    /**
     * Get the questions eligible for serialization.
     *
     * @return ArrayList of Questions.
     */
    ArrayList<Question> getQuestionsForSerialization() {
        ArrayList<Question> allQuestions = new ArrayList<>();
        for (Module module : QuizData.getQuestionModules()) {
            for (Question question : module.questions) {
                if (!"".equals(question.getResponse())) {
                    allQuestions.add(question);
                }
            }
        }
        return allQuestions;
    }

    /**
     * Function to submit the current quiz to server.
     */
    public void submitQuiz() {
        SafeApplication.getJobManager().addJobInBackground(new QuizSubmissionJob());
    }

    /**
     * Function to try and invoke periodic submissions.
     * Periodic submissions will be processed only if the
     * time difference between successive calls is more than
     * {@link QuizSubmissions#PERIODIC_SUBMISSION_INTERVAL} seconds.
     */
    public void tryPeriodicSubmission() {
        if (periodicSubmissionQueued) {
            // Already a submission is going on. Try this later.
            retry = true;
            return;
        }

        periodicSubmissionQueued = true;
        SafeApplication.getJobManager()
                .addJobInBackground(new PeriodicSubmissionJob(getQuestionsForSerialization()));
    }

    /**
     * Function called after periodic submission is successful or failed.
     */
    public void postPeriodicSubmission() {
        // We need to retry. Add a job after the interval.
        // Note that we are removing the queue after the delay.
        // This makes sure that there is at least PERIODIC_SUBMISSION_INTERVAL
        // gap between two submissions.
        QuizData.getQuizActivity().runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                periodicSubmissionQueued = false;

                                if (!retry) {
                                    return;
                                }
                                retry = false;
                                tryPeriodicSubmission();
                            }
                        }, PERIODIC_SUBMISSION_INTERVAL * 1000);
                    }
                }
        );
    }
}


/**
 * Periodic Submission job which sends the data to server in
 * the background using web sockets. {@link QuizSubmissions#postPeriodicSubmission()}
 * is called after the job is successful or if failed completely.
 */
@SuppressWarnings("CheckStyle")
class PeriodicSubmissionJob extends Job {

    public static final int PRIORITY = 1;

    private final ArrayList<Question> questions;

    public PeriodicSubmissionJob(ArrayList<Question> questions) {
        // This job requires network connectivity,
        super(new Params(PRIORITY).requireNetwork().groupBy("periodicSubmission"));
        this.questions = questions;
    }

    String createJson() {
        Map<String, Object> periodicSubmissionMap = new HashMap<>();
        periodicSubmissionMap.put("url", AppConstant.QUIZ_PERIODIC_SUBMIT_URL);
        periodicSubmissionMap.put("data", questions);

        Gson gson = QuizSubmissions.getQuizResponseGson();
        return gson.toJson(periodicSubmissionMap);
    }

    @Override
    public void onAdded() {
        // Job has been saved to disk.
        // This is a good place to dispatch a UI event to
        // indicate the job will eventually run.
    }

    @Override
    public void onRun() throws Throwable {
        // Job Logic goes here
        logDebug(logTag(), "Sending periodic submission");
        String dataToBeSent = createJson();

        if (dataToBeSent.equals(QuizSubmissions.instance.oldSubmissionData)) {
            logDebug(logTag(), "Periodic submission is the same as previous. Aborting");
            return;
        }

        if (!SafeWebSocket.getInstance().pushToServer(dataToBeSent)) {
            if (QuizData.getQuizActivity() == null) {
                logDebug(logTag(), "Looks like quiz is ended. Cancelling periodic submission");
                return;
            }
            throw new Exception();
        }
        QuizSubmissions.instance.oldSubmissionData = dataToBeSent;
        QuizSubmissions.instance.postPeriodicSubmission();
        logDebug(logTag(), "Periodic submission successful");
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        logError(logTag(), "Too many retries for sending the log: " + createJson());
        QuizSubmissions.instance.postPeriodicSubmission();
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(
            @NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, 20);
    }
}


/**
 * Job for submission of the quiz to server.
 */
@SuppressWarnings("CheckStyle")
class QuizSubmissionJob extends Job {

    public static final int PRIORITY = 10;
    String dataToBeSent;
    String url;

    protected QuizSubmissionJob() {
        // This must be persisted because we need to try to submit it even
        // if the app crashes.
        super(new Params(PRIORITY).requireNetwork().persist().groupBy("quizSubmission"));
        dataToBeSent = QuizSubmissions.getQuizResponseGson()
                .toJson(QuizSubmissions.instance.getQuestionsForSerialization());
        url = AppConstant.getBaseUrl() + AppConstant.QUIZ_SUBMIT_URL;
        url = String.format(Locale.ENGLISH, url, QuizData.getQuizUuid());
    }

    @Override
    public void onAdded() {
        // Job has been saved to disk.
        // This is a good place to dispatch a UI event to
        // indicate the job will eventually run.
        LoadingScreen.getInstance().addDebugInfo("Submission job added");
    }

    @Override
    public void onRun() throws Throwable {
        // Job Logic goes here
        Response httpResponse = ServerInterface.getHttpResponse(url, dataToBeSent, null);

        if (httpResponse == null || httpResponse.code() != 200) {
            if (httpResponse != null) {
                logError(logTag(), "Submission: Error in http response."
                        + httpResponse.body().string());
            } else {
                logError(logTag(), "Submission: HttpResponse is null");
            }
            throw new Exception();
        }

        LoadingScreen.getInstance().setCountDownDisplay("Submitted Successfully");
        LoadingScreen.getInstance().addDebugInfo("Submission Complete");
        QuizData.getQuizActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                AppData.setLoginToken(null);
                                AppData.setQuizStarted(false);
                                QuizData.getQuizActivity().setQuizStateToNotStarted();
                                QuizData.getQuizActivity().finish();
                            }
                        }, 1000);
            }
        });
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        logError(logTag(), "Too many retries for sending the final submission");
        QuizData.setQuizFrozen(true);
        QuizLoader.getInstance().onPageChangeListener.onPageSelected(
                QuizLoader.getInstance().quizPager.getCurrentItem()
        );
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(
            @NonNull Throwable throwable, int runCount, int maxRunCount) {
        // TODO: Freeze while we are trying to send the data in background beyond a few tries.
        return RetryConstraint.createExponentialBackoff(runCount, 20);
    }
}
