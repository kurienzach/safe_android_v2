package com.iitb.cse.arkenstone.safe.base;

import static org.acra.ReportField.ANDROID_VERSION;
import static org.acra.ReportField.APPLICATION_LOG;
import static org.acra.ReportField.APP_VERSION_CODE;
import static org.acra.ReportField.APP_VERSION_NAME;
import static org.acra.ReportField.AVAILABLE_MEM_SIZE;
import static org.acra.ReportField.BRAND;
import static org.acra.ReportField.BUILD;
import static org.acra.ReportField.BUILD_CONFIG;
import static org.acra.ReportField.CRASH_CONFIGURATION;
import static org.acra.ReportField.CUSTOM_DATA;
import static org.acra.ReportField.DEVICE_FEATURES;
import static org.acra.ReportField.DEVICE_ID;
import static org.acra.ReportField.DISPLAY;
import static org.acra.ReportField.DROPBOX;
import static org.acra.ReportField.DUMPSYS_MEMINFO;
import static org.acra.ReportField.ENVIRONMENT;
import static org.acra.ReportField.EVENTSLOG;
import static org.acra.ReportField.FILE_PATH;
import static org.acra.ReportField.INITIAL_CONFIGURATION;
import static org.acra.ReportField.INSTALLATION_ID;
import static org.acra.ReportField.IS_SILENT;
import static org.acra.ReportField.LOGCAT;
import static org.acra.ReportField.MEDIA_CODEC_LIST;
import static org.acra.ReportField.PACKAGE_NAME;
import static org.acra.ReportField.PHONE_MODEL;
import static org.acra.ReportField.PRODUCT;
import static org.acra.ReportField.REPORT_ID;
import static org.acra.ReportField.SETTINGS_GLOBAL;
import static org.acra.ReportField.SETTINGS_SECURE;
import static org.acra.ReportField.SETTINGS_SYSTEM;
import static org.acra.ReportField.SHARED_PREFERENCES;
import static org.acra.ReportField.STACK_TRACE;
import static org.acra.ReportField.THREAD_DETAILS;
import static org.acra.ReportField.TOTAL_MEM_SIZE;
import static org.acra.ReportField.USER_APP_START_DATE;
import static org.acra.ReportField.USER_COMMENT;
import static org.acra.ReportField.USER_CRASH_DATE;
import static org.acra.ReportField.USER_EMAIL;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import android.app.Application;
import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.scheduling.GcmJobSchedulerService;
import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.services.GcmJobService;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.Logging;
import com.iitb.cse.arkenstone.safe.utils.NetworkUtils;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * ACRA settings to report crashes to server.
 */
@ReportsCrashes(
        formUri = AppConstant.DEFAULT_BASE_URL + AppConstant.ACRA_ERROR_URL,
        customReportContent = { REPORT_ID, APP_VERSION_CODE, APP_VERSION_NAME, PACKAGE_NAME,
                FILE_PATH, PHONE_MODEL, BRAND, PRODUCT, ANDROID_VERSION, BUILD, TOTAL_MEM_SIZE,
                AVAILABLE_MEM_SIZE, BUILD_CONFIG, CUSTOM_DATA, IS_SILENT, STACK_TRACE,
                INITIAL_CONFIGURATION, CRASH_CONFIGURATION, DISPLAY, USER_COMMENT, USER_EMAIL,
                USER_APP_START_DATE, USER_CRASH_DATE, DUMPSYS_MEMINFO, LOGCAT,
                INSTALLATION_ID, DEVICE_FEATURES, ENVIRONMENT, SHARED_PREFERENCES, DEVICE_ID,
                MEDIA_CODEC_LIST, SETTINGS_GLOBAL, SETTINGS_SECURE, SETTINGS_SYSTEM, THREAD_DETAILS,
                EVENTSLOG, DROPBOX, APPLICATION_LOG,
        },
        excludeMatchingSharedPreferencesKeys = {"password"},
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text,
        alsoReportToAndroidFramework = true)

public class SafeApplication extends Application {

    private static JobManager jobManager;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ACRA.init(this);
    }

    @Override
    public final void onCreate() {
        // Should we use ACRA#isACRASenderServiceProcess?
        super.onCreate();
        createSingletons();

        // Setup Android Priority Job Queue
        Configuration.Builder builder = new Configuration.Builder(this)
                .minConsumerCount(1)
                .maxConsumerCount(3);
        int enableGcm = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (enableGcm == ConnectionResult.SUCCESS) {
            builder.scheduler(GcmJobSchedulerService.createSchedulerFor(this,
                    GcmJobService.class), false);
        }
        Configuration configuration = builder.build();
        jobManager = new JobManager(configuration);
    }

    /**
     * Instantiate all singletons in the
     * application scope.
     */
    private void createSingletons() {
        ContextHelper.createInstance(this);
        Logging.createInstance(this);
        NetworkUtils.createInstance(this);
    }

    public static JobManager getJobManager() {
        return jobManager;
    }
}
