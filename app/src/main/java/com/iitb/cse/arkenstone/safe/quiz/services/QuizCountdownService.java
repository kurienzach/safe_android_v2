package com.iitb.cse.arkenstone.safe.quiz.services;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * A service that manages the countdown for quiz.
 * This is a different implementation from the previous
 * application because we want to run the countdown
 * even when the user tries to go out of the application.
 * This is run as a service so that it is not stopped
 * when user tries to navigate out of context.
 * The service sends broadcasts every second and that
 * is used for updating the UI timer.
 */
public class QuizCountdownService extends Service {

    public static final String COUNTDOWN_BR = "com.iitb.cse.arkenstone.quiz_countdown_br";
    Intent broadcastIntent = new Intent(COUNTDOWN_BR);

    CountDownTimer timer = null;

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        long timeLeft = intent.getLongExtra("timeLeft", -1);

        // Timer is not null means that the timer is already started.
        // So, ignore that.
        if (timer == null) {
            logDebug(logTag(), "Starting quiz timer: " + timeLeft / 1000 + " seconds");

            // Infinite quiz. Return a negative value.
            if (timeLeft == -1) {
                logDebug(logTag(), "Looks like a quiz with no end time!");
                broadcastIntent.putExtra("countdown", -1);
                sendBroadcast(broadcastIntent);

                return START_STICKY;
            }

            timer = new CountDownTimer(timeLeft, 1000) {
                @Override
                public void onTick(long millisRemaining) {
                    broadcastIntent.putExtra("countdown", millisRemaining);
                    sendBroadcast(broadcastIntent);
                }

                @Override
                public void onFinish() {
                    logDebug(logTag(), "Countdown complete");
                    // Return a zero on quiz complete.
                    broadcastIntent.putExtra("countdown", 0L);
                    sendBroadcast(broadcastIntent);
                }
            };
            timer.start();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        timer.cancel();
        timer = null;
        logDebug(logTag(), "Timer cancelled");

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
