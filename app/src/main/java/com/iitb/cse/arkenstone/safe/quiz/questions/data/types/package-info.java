/**
 * This package contains files for serializing
 * and deserializing different types of questions.
 */

package com.iitb.cse.arkenstone.safe.quiz.questions.data.types;
