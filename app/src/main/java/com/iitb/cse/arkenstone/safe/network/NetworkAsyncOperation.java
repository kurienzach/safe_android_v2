package com.iitb.cse.arkenstone.safe.network;

import com.iitb.cse.arkenstone.safe.utils.TimeUtil;

/**
 * Implement this interface for classes which would call the
 * server in order to accept the return data from server.
 * getTimeUtil is required because every class which implements this
 * interface need will call NetworkOperation Async Task and it
 * requires TimeUtil object
 */
public interface NetworkAsyncOperation {

    /**
     * Return a TimeUtil for use in timings for Network Operation.
     * @return TimeUtil instance.
     */
    TimeUtil getTimeUtil();

    /**
     * Callback function that is called with the response from server.
     * @param tag A tag used to identify the caller.
     * @param response Response from the server.
     * @param responseCode Response Code from server. null if there is an unknown error
     */
    void doneNetworkOperation(String tag, String response, Integer responseCode);
}
