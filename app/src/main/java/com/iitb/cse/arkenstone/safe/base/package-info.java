/**
 * This package contains classes which act as
 * base classes for several parts of the application.
 */

package com.iitb.cse.arkenstone.safe.base;
