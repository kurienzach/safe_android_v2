package com.iitb.cse.arkenstone.safe.utils.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Handler;

import com.iitb.cse.arkenstone.safe.quiz.QuizLogging;
import com.iitb.cse.arkenstone.safe.quiz.services.QuizMonitorService;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

/**
 * Listen for call state changes
 */
public class CallListener extends BroadcastReceiver{

    Context mContext;

    private static MyPhoneStateListener PhoneListener = null;

    private static int phoneState = 0;

//    private static Timer timer;
//    private static TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
//    final Handler handler = new Handler();

    public static int getPhoneState() {
        return phoneState;
    }

    public void onReceive(Context context, Intent intent) {
        mContext = context;

        try {
            // TELEPHONY MANAGER class object to register one listener
            TelephonyManager tmgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            //Create Listener
            if(PhoneListener == null) {
                PhoneListener = new MyPhoneStateListener();
                // Register listener for LISTEN_CALL_STATE
                tmgr.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
            }

        } catch (Exception e) {
            logError(logTag() + " Phone Receive Error", e.getMessage());
            e.printStackTrace();
        }
    }

    private class MyPhoneStateListener extends PhoneStateListener {

        public void onCallStateChanged(int state, String incomingNumber) {

            //logDebug(logTag(), "QuizApp", state + "   incoming no:" + incomingNumber);

//            if (AppData.getQuizId() == null || AppData.getQuizSelectionResponse() == null) {
//                return;
//            }

//            if (!QuizMonitorService.isRunning()) {
//                //Quiz may not be progressing
//                return;
//            }

            logDebug(logTag(), "QuizApp", state + "   incoming no:" + incomingNumber);

            logDebug(logTag(), "timer stopped from phone state listener start");

            String msg = null;
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    //msg = "Phone is ringing.";
                    phoneState = 1;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    //msg = "Phone in Call (Call accepted)";
                    phoneState = 2;
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    logDebug(logTag(), "From call state idle stop");
                    phoneState = 0;
                    break;
                default:
                    return;
            }



            //Check network availability

//            if (mContext != null && msg != null) {
//                logDebug(logTag(),"context is null");
//                QuizLogging.newLog(new QuizLogging.QuizLog(msg, QuizLogging.QuizLog.Severity.CRITICAL));
//            }
        }
    }
}