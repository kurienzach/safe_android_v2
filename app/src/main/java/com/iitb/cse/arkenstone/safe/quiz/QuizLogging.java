package com.iitb.cse.arkenstone.safe.quiz;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.iitb.cse.arkenstone.safe.base.SafeApplication;
import com.iitb.cse.arkenstone.safe.network.SafeWebSocket;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for logging information to the server during
 * a quiz. This class uses the SafeWebSocket for sending
 * the data.
 */
@SuppressWarnings("unused")
public class QuizLogging {
    private static QuizLogging instance = new QuizLogging();

    /**
     * Private constructor to defeat instantiating.
     */
    private QuizLogging() {
        // No body required.
    }

    public static class QuizLog implements Serializable {

        public enum Severity implements Serializable {
            INFO, WARNING, CRITICAL, DEBUG_PHONE
        }

        Severity severity;
        String data;

        public QuizLog(String data) {
            this.data = data;
            this.severity = Severity.INFO;
        }

        public QuizLog(String data, Severity severity) {
            this.data = data;
            this.severity = severity;
        }

        /**
         * Get the JSON representation for the Log.
         *
         * @return A string representing the JSON.
         */
        public String createJson() {
            Map<String, Object> quizLogMap = new HashMap<>();
            quizLogMap.put("url", AppConstant.QUIZ_LOG_URL);

            Map<String, Object> data = new HashMap<>();
            data.put("severity", this.severity.name());

            Map<String, String> actualData = new HashMap<>();
            actualData.put("message", this.data);

            data.put("data", actualData);
            quizLogMap.put("data", data);

            Gson gson = new Gson();
            return gson.toJson(quizLogMap);
        }
    }

    /**
     * Create a new log and queue it for sending to the server.
     *
     * @param quizLog The quiz log object which is required to be sent to the server.
     */
    public static void newLog(QuizLog quizLog) {
        SafeApplication.getJobManager().addJobInBackground(new PostLogJob(quizLog));
    }
}

/**
 * Severity for logs.
 */
@SuppressWarnings({"CheckStyle", "unused"})
enum Severity {
    INFO,
    WARNING,
    CRITICAL,
    DEBUG_PHONE // Used for debugging purposes.
}


@SuppressWarnings("CheckStyle")
class PostLogJob extends Job {
    public static final int PRIORITY = 1;
    QuizLogging.QuizLog log;

    public PostLogJob(QuizLogging.QuizLog log) {
        // This job requires network connectivity,
        super(new Params(PRIORITY).requireNetwork().groupBy("id"));
        this.log = log;
    }

    @Override
    public void onAdded() {
        // Job has been saved to disk.
        // This is a good place to dispatch a UI event to
        // indicate the job will eventually run.
    }

    @Override
    public void onRun() throws Throwable {
        // Job Logic goes here
        if (!SafeWebSocket.getInstance().pushToServer(log.createJson())) {
            throw new Exception();
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        logError(logTag(), "Too many retries for sending the log: " + log.createJson());
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(
            @NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, 100);
    }
}
