package com.iitb.cse.arkenstone.safe.quiz;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.iitb.cse.arkenstone.safe.R;

/**
 * Class for control and working of the Floating Action Button/Menu
 * for QuizApp.
 */
public final class QuizFab {

    FloatingActionMenu fam = null;
    FloatingActionButton clearReason = null;
    FloatingActionButton clearOther = null;

    public static final QuizFab instance = new QuizFab();

    /**
     * Private constructor to defeat instantiation.
     */
    private QuizFab() {
        // No body needed
    }

    /**
     * Setup the Quiz Floating action button/menu for the quiz ui.
     *
     * @param activity Activity in which the bottom bar is displayed.
     */
    public void setupQuizFab(final QuizActivity activity) {
        fam = (FloatingActionMenu) activity.findViewById(R.id.fam);

        // FAM should be above the bottom navigation
        AHBottomNavigation bottomNavigation = BottomNavigation.instance.bottomNavigation;
        CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(
                0,
                0,
                Math.round(8 * activity.getResources().getDisplayMetrics().density),
                Math.round(8 * activity.getResources().getDisplayMetrics().density)
                        + bottomNavigation.getMeasuredHeight()
        );
        params.gravity = Gravity.BOTTOM | Gravity.END;
        fam.setLayoutParams(params);

        // Create the clear reason FAB
        clearReason = new FloatingActionButton(activity);
        clearReason.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        clearReason.setButtonSize(FloatingActionButton.SIZE_MINI);
        clearReason.setImageResource(R.drawable.ic_eraser_variant);
        clearReason.setColorNormalResId(R.color.colorPrimary);
        clearReason.setColorPressedResId(R.color.colorPrimaryDark);
        clearReason.setLabelText("Clear Reason Text");

        // Create the clear other FAB
        clearOther = new FloatingActionButton(activity);
        clearOther.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        clearOther.setButtonSize(FloatingActionButton.SIZE_MINI);
        clearOther.setImageResource(R.drawable.ic_eraser_variant);
        clearOther.setColorNormalResId(R.color.colorPrimary);
        clearOther.setColorPressedResId(R.color.colorPrimaryDark);
        clearOther.setLabelText("Clear Selected Option");

        fam.hideMenuButton(false);
        fam.setClosedOnTouchOutside(true);
    }

    /**
     * Display the FAB on the screen.
     */
    public void showFab() {
        fam.showMenuButton(true);
    }

    /**
     * Hide the fab from the screen.
     */
    public void hideFab() {
        if (fam != null) {
            fam.hideMenuButton(true);
        }
    }

    /**
     * Collapse the FAB. Hide the FAB if expanded.
     */
    public void collapseFab() {
        fam.close(true);
    }

    public FloatingActionButton getClearReasonBtn() {
        return clearReason;
    }

    public FloatingActionButton getClearOtherBtn() {
        return clearOther;
    }

    /**
     * Remove all the menu buttons from the Floating Action Menu.
     */
    public void resetFab() {
        fam.removeAllMenuButtons();
    }

    /**
     * Add a button to the Floating Action Menu. The buttons are generally
     * are taken using {@link QuizFab#getClearReasonBtn()} and
     * {@link QuizFab#getClearOtherBtn()}
     *
     * @param fab The action button.
     */
    public void addButton(FloatingActionButton fab) {
        fam.addMenuButton(fab);
    }

    /**
     * Get the top position of the quiz fab measured from the top of bottom bar.
     *
     * @return Top position in pixels with respect to the parent.
     */
    @NonNull
    public Integer getHeightFromBottom() {
        fam.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        return ((CoordinatorLayout.LayoutParams) fam.getLayoutParams()).bottomMargin
                + fam.getMeasuredHeight();
    }
}
