package com.iitb.cse.arkenstone.safe.utils;

import android.app.Application;
import android.support.v4.BuildConfig;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 * Custom log class for debugging purposes.
 */
public final class Logging {

    /**
     * Instance for the singleton class.
     */
    private static Logging instance = null;

    /**
     * Application instance used to access files.
     */
    private static Application application;

    /**
     * Private constructor to defeat instantiation.
     */
    private Logging() {
        // Exists only to defeat instantiation.
    }

    /**
     * Create the singleton instance if it does not exist.
     *
     * @param app The application instance
     */
    public static void createInstance(final Application app) {
        instance = new Logging();
        application = app;
    }

    /**
     * Log a debug message.
     *
     * @param tag     The tag for the message.
     * @param extra   Any extra details for the tag
     * @param message The debug message
     */
    public static void logDebug(final String tag,
                                final String extra,
                                final String message) {
        if (tag == null || message == null || extra == null) {
            return;
        }
        appendLog(tag + " " + extra + ": " + message);
        Log.d(tag + " " + extra, message);
    }

    /**
     * Log a debug message.
     *
     * @param tag     The tag for the message
     * @param message The debug message
     */
    public static void logDebug(final String tag,
                                final String message) {
        if (tag == null || message == null) {
            return;
        }
        appendLog(tag + ": " + message);
        Log.d(tag, message);
    }

    /**
     * Log a debug message.
     *
     * @param tag       The tag for the message
     * @param exception The exception to be logged
     */
    public static void logDebug(final String tag,
                                final Exception exception) {
        if (tag == null || exception == null) {
            return;
        }
        String message = Arrays.toString(exception.getStackTrace());
        appendLog(tag + ": " + message);
        Log.d(tag, message);
    }

    /**
     * Log a warning message.
     *
     * @param tag     The tag for the message.
     * @param message The error message
     */
    public static void logWarning(final String tag,
                                  final String message) {
        if (tag == null || message == null) {
            return;
        }
        appendLog(tag + ": " + message);
        Log.w(tag, message);
    }

    /**
     * Log an error message.
     *
     * @param tag     The tag for the message.
     * @param message The error message
     */
    public static void logError(final String tag,
                                final String message) {
        if (tag == null || message == null) {
            return;
        }
        appendLog(tag + ": " + message);
        Log.e(tag, message);
    }

    /**
     * Log an error message.
     *
     * @param tag       The tag for the message.
     * @param exception The exception to be logged as an error.
     */
    public static void logError(final String tag,
                                final Exception exception) {
        if (tag == null || exception == null) {
            return;
        }
        String message = Arrays.toString(exception.getStackTrace());
        appendLog(tag + ": " + message);
        Log.e(tag, message);
    }

    /**
     * Append to the log file. Logging is done only if debug is enabled
     *
     * @param text Log to be appended
     */
    public static void appendLog(final String text) {

        //noinspection PointlessBooleanExpression
        if (!BuildConfig.DEBUG) {
            return;
        }

        File logFile = new File(application.getFilesDir(),
                AppConstant.LOG_FILE);

        if (!logFile.exists()) {
            //noinspection ResultOfMethodCallIgnored
            logFile.mkdirs();
        }


        // Create file if it does not exist
        if (!logFile.isFile()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                logFile.createNewFile();
            } catch (IOException e) {
                Log.e("FAILED IO " + AppConstant.LOG_FILE, e.getMessage());
            }
        }

        try {
            BufferedWriter buf = new BufferedWriter(
                    new FileWriter(logFile, true));
            buf.append("---------------------------------------------------\n");
            buf.append("---------------------------------------------------\n");
            buf.append(String.valueOf(System.currentTimeMillis())).append("\n");
            buf.append("---------------------------------------------------\n");
            buf.append(text).append("\n");
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Append an exception trace to the logging file.
     *
     * @param exception Exception to be appended to log file.
     */
    private static void appendLog(final Exception exception) {
        appendLog(Arrays.toString(exception.getStackTrace()));
    }

    /**
     * Create a tag that contains the class name and line
     * number. Useful in debugging to pinpoint the root
     * cause of an issue.
     *
     * @return A tag to be used for logging.
     */
    public static String logTag() {
        String tag = "";
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        for (int i = 0; i < ste.length; i++) {
            if ("logTag".equals(ste[i].getMethodName())) {
                tag = "SafeLog (" + ste[i + 1].getFileName()
                        + ":" + ste[i + 1].getLineNumber() + ")";
            }
        }
        return tag;
    }
}
