package com.iitb.cse.arkenstone.safe.utils;

import android.util.Log;

/**
 * This singleton class contains all the constants used in the application.
 */
public final class AppConstant {

    public static final String BASE_URL = "http://10.129.1.153:8000/";
    /**
     * Default fall back base url.
     */
    //public static final String DEFAULT_BASE_URL = "http://bodhitree3.cse.iitb.ac.in:9100/";
    public static final String DEFAULT_BASE_URL = BASE_URL;

    /**
     * URL for sending errors from ACRA.
     */
    public static final String ACRA_ERROR_URL = "api/acra/log/";

    /**
     *  File name to store logs.
     */
    public static final String LOG_FILE = "Safe.log";

    /**
     * Base URL for all server calls.
     */
    private static String baseUrl = BASE_URL+"api/";

    /**
     * Root URL.
     */
    private static String rootUrl = BASE_URL;//"http://bodhitree3.cse.iitb.ac.in:9100/";

    /**
     * API part for all server calls.
     */
    public static String API_SUFFIX_URL = "api/";

    /**
     * Should accept all certificates or not.
     */
    private static Boolean acceptCertificates = false;


    /***** Urls and constants for Login data. **/

    /**
     * The login URL suffix for logging in of the user.
     */
    public static final String LOGIN_URL = "account/login/";

    /**
     * Organization field for Login.
     */
    public static final String LOGIN_ORG = "organization";

    /**
     * Username field for Login.
     */
    public static final String LOGIN_USERNAME = "username";

    /**
     * Password field for Login.
     */
    public static final String LOGIN_PASSWORD = "password";

    /**
     * URL for getting organization list.
     */
    public static final String ORG_LIST_URL = "organization/list/";

    /***** Urls and constants for Login data end. **/

    /***** Constants for settings screen.            **/

    /**
     * Key for server url.
     */
    public static final String PREF_SERVER_URL = "server_url";

    /**
     * Key for show instructions preference.
     */
    public static final String PREF_SHOW_INSTRUCTIONS = "pref_show_instructions";

    /***** Constants for settings screen end.          **/

    /***** Constants for requesting permission.        **/

    /**
     * Request code to ask permissions.
     */
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;

    /**
     * System overlay permission.
     */
    public static final int REQUEST_CODE_OVERLAY_PERMISSION = 125;

    /**
     * Request code to display settings for permissions.
     */
    public static final int REQUEST_PERMISSION_SETTING = 124;

    /***** Constants for requesting permission end.    **/

    /***** Constants for Fetch and Get quiz. *************/

    /**
     * URL for authenticating for a quiz.
     */
    public static final String QUIZ_AUTH_URL = "quiz/%s/auth/";

    /**
     * URL for fetching a quiz.
     */
    public static final String QUIZ_FETCH_URL = "quiz/%s/fetch/";

    /**
     * URL for submitting a quiz.
     */
    public static final String QUIZ_SUBMIT_URL = "quiz/%s/submit/";

    /**
     * URL for web sockets.
     */
    public static final String WEB_SOCKET_URL = "ws/%s/?token=%s";

    /***** Constants for Fetch and Get quiz end. *********/

    /***** Constants for okhttp.                       **/

    /**
     * HTTP 400: Bad Request.
     */
    public static final int HTTP_BAD_REQUEST = 400;

    /**
     * HTTP 200 : OK.
     */
    public static final int HTTP_OK = 200;

    /***** Constants for okhttp end.                   **/

    /************* Constants for WebSockets. *************/

    public static final String QUIZ_LOG_URL = "INSERT_LOG";

    public static final String QUIZ_PERIODIC_SUBMIT_URL = "PERIODIC_SUBMISSION";

    /***** Constants for Websockets end.                **/

    /************* Constants for Previous submissions. ****/

    public static final String SHORT_SUBMISSION_HISTORY_URL = "quiz/submissions/";

    public static final String QUIZ_SUBMISSION_DETAIL_URL = "quiz/submissions/%s/detail/";

    /***** Constants for previous submissions end.       **/

    /************* Constants for question types. *********/

    public static final String TYPE_MCQ_SINGLE_ANSWER = "SINGLE_CORRECT_ANSWER";

    public static final String TYPE_MCQ_MULTIPLE_ANSWER = "MULTIPLE_CHOICE";

    public static final String TYPE_INTEGER_ANSWER = "INTEGER_ANSWER";

    public static final String TYPE_FLOAT_ANSWER = "FLOAT_ANSWER";

    public static final String TYPE_TEXT_ANSWER = "TEXT_ANSWER";

    /************* Constants for question types end. *****/

    /************* Monitor enum **************************/

    public static final int MSG_CONNECTED = 1;
    public static final int MSG_DISCONNECTED = 2;
    public static final int MSG_UI_OUT_OF_FOCUS = 3;
    public static final int MSG_UI_IN_FOCUS = 4;

    /************* Monitor enum end **********************/

    /***** Functions for accessing data.               **/

    /**
     * Get the value of base URL.
     * @return Base URL for the application
     */
    public static String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Set the base URL to a value.
     * @param baseUrlNew The new value of the base URL.
     */
    public static void setBaseUrl(final String baseUrlNew) {
        AppConstant.baseUrl = baseUrlNew;
    }

    /**
     * Exists only to defeat instantiation.
     */
    private AppConstant() {
        // Body not needed
    }

    /**
     * Get the status of accepting all certificates for TLS.
     * @return true if yes, false otherwise.
     */
    public static Boolean getAcceptCertificates() {
        return acceptCertificates;
    }

    /**
     * Set the status of acceptance of all certificates.
     * @param acceptCertificates The status of acceptance of all certificates.
     */
    public static void setAcceptCertificates(Boolean acceptCertificates) {

        if (acceptCertificates) {
            Log.w("Safe Settings", "Accept all certificates is true. This might render the"
                    + "HTTPS connection unsafe");
        }

        AppConstant.acceptCertificates = acceptCertificates;
    }

    /**
     * Get the root url.
     * @return Root url as string
     */
    public static String getRootUrl() {
        return rootUrl;
    }

    /**
     * Set the rootUrl.
     * @param rootUrl Value of rootUrl.
     */
    public static void setRootUrl(String rootUrl) {
        AppConstant.rootUrl = rootUrl;
    }
}
