package com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types;

import android.graphics.Color;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.github.clans.fab.FloatingActionButton;
import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.QuizFab;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.IntegerAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.QuestionFragment;
import com.iitb.cse.arkenstone.safe.utils.AppData;

/**
 * Fragment for integer answer type question.
 */

public class IntegerAnswerFrag extends QuestionFragment {
    /**
     * The question object for accessing its details.
     */
    IntegerAnswer fullQuestion;
    EditText input;

    @Override
    public void generateChildView(View inflatedLayout) {
        fullQuestion = (IntegerAnswer) question;

        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        input = new EditText(getContext());
        input.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setHint("Enter integer response");
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence sequence, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence sequence, int start, int before, int count) {
                // TODO Change answer on text change

            }
        });

        if (fullQuestion.answers != null) {
            // If answer is available (in case of PreviousQuizActivity)
            input.setText(String.valueOf(fullQuestion.submittedResponse.get(0)));
            for (Integer answer : fullQuestion.answers) {
                if (fullQuestion.submittedResponse.get(0).equals(answer)) {
                    input.setTextColor(Color.GREEN);
                }
            }
            input.setEnabled(false);
            layout.addView(input);

            // Add web view for Answer display
            String answers = "<span style=\"color:green\"> <b><center>ANSWER: </center>";
            for (int i = 0; i < fullQuestion.answers.size(); i++) {
                if (i == 0) {
                    answers += fullQuestion.answers.get(0);
                } else {
                    answers += " OR " + fullQuestion.answers.get(i);
                }
            }

            final WebView answerWebView = AppData.getWebViewCache()
                    .get("<span hidden>" + question.id + "</span>" + answers);
            answerWebView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            answerWebView.setEnabled(false);
            layout.addView(answerWebView);
        } else {
            layout.addView(input);
        }


        LinearLayout layoutDisplay = (LinearLayout) inflatedLayout.findViewById(R.id.frag_question);
        layoutDisplay.addView(layout);


    }

    @Override
    public void setupFab() {
        QuizFab.instance.resetFab();

        if (question.reasonTextLength != 0) {
            //TODO fab for reason text
            QuizFab.instance.addButton(QuizFab.instance.getClearReasonBtn());
        }

        final FloatingActionButton clearOtherBtn = QuizFab.instance.getClearOtherBtn();
        clearOtherBtn.setLabelText("Clear Options");
        clearOtherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.setText("");
                QuizFab.instance.collapseFab();
            }
        });
        QuizFab.instance.addButton(clearOtherBtn);
    }

    @Override
    public void freezeQuestion() {
        input.setEnabled(false);
        QuizFab.instance.hideFab();
    }

}
