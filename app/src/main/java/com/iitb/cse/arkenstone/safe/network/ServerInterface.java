package com.iitb.cse.arkenstone.safe.network;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.utils.TimeUtil;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * The class that conducts the actual server calls.
 */
public final class ServerInterface {

    /**
     * The JSON MediaType used to send to the server.
     */
    public static final MediaType JSON =
            MediaType.parse("application/json; charset=utf-8");

    /**
     * Text MediaType used to send to the server using
     * web sockets.
     */
    public static final MediaType WEB_SOCKET_TEXT =
            MediaType.parse("application/vnd.okhttp.websocket+text; charset=utf-8");

    /**
     * Private constructor to prevent instantiation.
     */
    private ServerInterface() {
        // Body not required
    }

    /**
     * Called when url settings is changed by user.
     * Reloads the url from shared preferences and appends the API SUFFIX.
     *
     * @param context Context for accessing Shared Preferences
     **/
    public static void reloadServerBaseUrl(final Context context) {
        SharedPreferences sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(context);
        AppConstant.setRootUrl(sharedPrefs.getString(AppConstant.PREF_SERVER_URL,
                AppConstant.DEFAULT_BASE_URL));
        AppConstant.setBaseUrl(AppConstant.getRootUrl() + AppConstant.API_SUFFIX_URL);
    }

    /**
     * This function makes actual HTTP network call to server.
     *
     * @param url      url to send request
     * @param jsonData json body if any to be send to server
     * @param util     used to profile server response to this request
     * @return server response object
     */
    public static Response getHttpResponse(final String url,
                                           final String jsonData,
                                           final TimeUtil util) {
        logDebug(logTag(), url);

        try {

            Request.Builder builder = new Request.Builder();
            builder.url(url);

            if (jsonData == null) {
                builder = builder.get();
            } else {
                RequestBody requestBody = RequestBody.create(JSON, jsonData);
                builder = builder.post(requestBody);
            }

            if (AppData.getLoginToken() != null) {
                builder.header("Authorization", "Token " + AppData.getLoginToken());
            }

            Request request = builder.build();

            if (util != null) {
                util.setStartTime();
            }

            OkHttpClient client = getHttpClientBuilder(AppConstant.getAcceptCertificates()).build();
            Response response = client.newCall(request).execute();

            if (util != null) {
                util.setEndTime();
            }

            // add tlv log in doneNetworkOperation method of caller

            MediaType type = response.body().contentType();
            logDebug(logTag(), type.toString());
            return response;

        } catch (Exception e) {
            // This error means that the call basically failed
            logError(logTag(), "HTTP Connection error!");
            e.printStackTrace();
            logError(logTag(), e);
        }
        return null;
    }

    /**
     * This is the heart of network communication.
     * This aysnc task does HTTP and calls the doneNetworkOperation in caller
     * Caller must implement NetworkAsyncOperation Interface
     */
    public static class NetworkOperation extends
            AsyncTask<Object, Integer, String> {

        /**
         * The networkAsyncOperation used for calling back with
         * response.
         */
        private NetworkAsyncOperation caller;

        /**
         * Tag from the caller to pass to the doneNetworkOperation.
         */
        private String tag;

        /**
         * Server response code if any.
         */
        private Integer responseCode;

        @Override
        protected final String doInBackground(final Object... params) {

            caller = (NetworkAsyncOperation) params[1];
            tag = (String) params[3];
            String jsonBody = (String) params[2];
            String url = (String) params[0];

            TimeUtil util;

            try {
                util = caller.getTimeUtil();
            } catch (Exception e) {
                util = new TimeUtil();
            }

            //Connect to server using HTTP Post
            Response response = getHttpResponse(url, jsonBody, util);

            if (response == null) {
                responseCode = null;
                return null;
            }

            String responseBody;

            try {
                responseCode = response.code();
                responseBody = response.body().string().trim();
                response.close();
                logDebug(logTag(), responseBody);
                return responseBody;
            } catch (IOException e) {
                e.printStackTrace();
                response.close();
                responseCode = null;
                return null;
            }
        }

        @Override
        protected final void onPostExecute(final String result) {
            if (caller != null) {
                caller.doneNetworkOperation(tag, result, responseCode);
            }
            //Caller must implement NetworkAsyncOperation Interface
        }
    }

    /**
     * Convert Request body to string for logging purposes.
     *
     * @param request OkHttp request object
     * @return String containing the body from Request.
     */
    @SuppressWarnings("unused")
    public static String okHttpBodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    /**
     * Get the Http Client Builder with the ssl configuration whether or not to
     * accept self signed certificates.
     *
     * @param sslAcceptAll Should accept all certificates or not (recommended: false)
     * @return OkHttpClientBuilder with the selected ssl setting.
     */
    public static OkHttpClient.Builder getHttpClientBuilder(boolean sslAcceptAll) {
        if (!sslAcceptAll) {
            return new OkHttpClient.Builder();
        }

        final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType)
                    throws CertificateException {
                // Not required
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType)
                    throws CertificateException {
                // Not required
            }
        }
        };

        SSLContext sslContext;

        try {
            sslContext = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            logError(logTag(), e);
            e.printStackTrace();
            return new OkHttpClient.Builder();
        }

        try {
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            logError(logTag(), e);
            e.printStackTrace();
            return new OkHttpClient.Builder();
        }
        OkHttpClient.Builder client = new OkHttpClient.Builder();

        //noinspection deprecation
        client.sslSocketFactory(sslContext.getSocketFactory());

        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @SuppressLint("BadHostnameVerifier")
            @Override
            public boolean verify(String hostname, SSLSession session) {
                logDebug(logTag(), "Trust Host :" + hostname);
                return true;
            }
        };
        client.hostnameVerifier(hostnameVerifier);
        return client;
    }
}
