/**
 * This package contains serializable java objects for
 * communication between server and client.
 */

package com.iitb.cse.arkenstone.safe.quiz.questions.data;
