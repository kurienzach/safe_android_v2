package com.iitb.cse.arkenstone.safe.quiz.questions.fragments.types;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.graphics.Color;
import android.support.annotation.IdRes;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.github.clans.fab.FloatingActionButton;
import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.QuizActivity;
import com.iitb.cse.arkenstone.safe.quiz.QuizFab;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.types.McqSingleAnswer;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.QuestionFragment;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.utils.widget.RadioGroupPlus;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Fragment for Multiple Choice question with
 * single answer.
 */
public class McqSingleAnswerFrag extends QuestionFragment
        implements RadioGroupPlus.OnCheckedChangeListener {

    /**
     * The question object for accessing its details.
     */
    McqSingleAnswer fullQuestion;

    /**
     * The buttons for options.
     */
    ArrayList<RadioButton> buttons = new ArrayList<>();

    /**
     * Group for containing the radio buttons.
     */
    volatile RadioGroupPlus radioGroup;

    @Override
    public void generateChildView(View inflatedLayout) {
        fullQuestion = (McqSingleAnswer) question;

        radioGroup = new RadioGroupPlus(getContext());
        radioGroup.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        for (String option : fullQuestion.shuffledOptions) {
            LinearLayout layoutForQuestion = new LinearLayout(getContext());
            layoutForQuestion.setOrientation(LinearLayout.HORIZONTAL);
            layoutForQuestion.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            final RadioButton button = new RadioButton(getContext());
            button.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            button.setId(View.generateViewId());
            button.setTag(fullQuestion.options.indexOf(option));

            // Bold and green correct answers
            if (fullQuestion.answers != null) {
                for (Integer answer: fullQuestion.answers) {
                    if (option.equals(fullQuestion.options.get(answer))) {
                        option = "<span style=\"color:green\"> <b>" + option + "</b></span>";
                    }
                }

            }

            final WebView optionWebView = AppData.getWebViewCache()
                    .get("<span hidden>" + question.id + "</span>" + option);
            optionWebView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));
            optionWebView.setPadding(0, 0, 15, 0);
            optionWebView.setId(View.generateViewId());
            optionWebView.setTag(button);
            optionWebView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (view.getId() == optionWebView.getId()
                            && motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        ((RadioButton) view.getTag()).performClick();
                    }
                    return false;
                }
            });
            if (optionWebView.getParent() != null) {
                ((ViewGroup) optionWebView.getParent()).removeView(optionWebView);
            }

            //For PreviousQuiz. Disable button and webView if submitted_response available
            if (fullQuestion.submittedResponse != null) {
                button.setEnabled(false);
                optionWebView.setEnabled(false);
            }

            buttons.add(button);
            layoutForQuestion.addView(button);

            layoutForQuestion.addView(optionWebView);
            radioGroup.addView(layoutForQuestion);
        }
        radioGroup.setOnCheckedChangeListener(this);

        LinearLayout layout = (LinearLayout) inflatedLayout.findViewById(R.id.frag_question);
        layout.addView(radioGroup);

        // If response is already available, select it.
        if (!"".equals(question.getResponse())) {
            Integer selectedOption = Integer.parseInt(question.getResponse());
            buttons.get(fullQuestion
                    .shuffledOptions
                    .indexOf(fullQuestion.options.get(selectedOption))).performClick();
        }

        //For PreviousQuiz. Click submitted response if available.
        if (fullQuestion.submittedResponse != null) {
                buttons.get(fullQuestion
                        .shuffledOptions
                        .indexOf(fullQuestion.options.get(fullQuestion.submittedResponse.get(0)))).performClick();
        }
    }

    @Override
    public void setupFab() {
        QuizFab.instance.resetFab();

        if (question.reasonTextLength != 0) {
            //TODO fab for reason text
            QuizFab.instance.addButton(QuizFab.instance.getClearReasonBtn());
        }

        final FloatingActionButton clearOtherBtn = QuizFab.instance.getClearOtherBtn();
        clearOtherBtn.setLabelText("Clear Options");
        clearOtherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioGroup.clearCheck();
                QuizFab.instance.collapseFab();
            }
        });
        QuizFab.instance.addButton(clearOtherBtn);
    }

    @Override
    public void freezeQuestion() {
        for (RadioButton button: buttons) {
            button.setEnabled(false);
        }
        QuizFab.instance.hideFab();
    }

    @Override
    public void onCheckedChanged(RadioGroupPlus group, @IdRes int checkedId) {
        // Disable check if PreviousQuiz
        if (fullQuestion.submittedResponse == null) {
            if (checkedId == -1) {
                question.setResponse("");
                logDebug(logTag(), "Question selection cleared");
                return;
            }

            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            Integer selectedOption = (Integer) rb.getTag();
            logDebug(logTag(), "Selected Option index is " + selectedOption);
            question.setResponse(String.valueOf(selectedOption));
        }
    }
}
