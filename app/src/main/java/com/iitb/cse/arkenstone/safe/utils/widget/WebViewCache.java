package com.iitb.cse.arkenstone.safe.utils.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.LruCache;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.iitb.cse.arkenstone.safe.quiz.QuizLoader;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

/**
 * Most parts of the quiz interface contains WebViews.
 * And all these WebViews require the same MathJax
 * libraries which might take time to load. Even
 * the rendering of the WebViews take time when instantiation.
 * <p></p>
 * We are providing a cache of WebViews which are preloaded
 * with the MathJax libraries and are ready to display any
 * type of information on the screen. This aims to reduce
 * the pressure on the cpu that is caused due to multiple
 * instantiation of WebViews in each and every screen.
 */
public class WebViewCache extends LruCache<String, WebView> {

    private final Context appContext;

    public static final int WebViewCacheSize = 50;

    /**
     * Semaphore for each web view. This is useful to know
     * when all the web views are loaded and ready to display
     */
    public static final Semaphore webViewCacheLock = new
            Semaphore(WebViewCache.WebViewCacheSize + 1);

    /**
     * Semaphore for the creation of webview. Make sure that the
     * webview creation is done before we start quiz interface.
     */
    public static final Semaphore webViewCacheCreate = new Semaphore(1);

    /**
     * WebViews which are free and not associated with
     * any display elements.
     */
    private final Queue<WebView> freeViews = new LinkedList<>();

    /**
     * Create the webview pool with several webviews at once.
     */
    public WebViewCache(Context context) {
        super(WebViewCacheSize);
        appContext = context;
        for (int i = 0; i < WebViewCacheSize; i++) {
            freeViews.add(generateWebViewForSafe());
        }
    }

    @Override
    protected final WebView create(String key) {
        WebView webView;

        webView = freeViews.poll();

        if (webView == null) {
            // Important: We add data to generate function here itself
            // because using setWebViewData can cause unknown behaviour
            // because of the time taken for the loading of scripts.
            return generateWebViewForSafe(key);
        }

        return setWebViewData(webView, key);
    }

    @Override
    protected void entryRemoved(
            boolean evicted, String key, WebView oldValue, WebView newValue) {
        freeViews.add(oldValue);
    }

    private WebView generateWebViewForSafe() {
        return generateWebViewForSafe("");
    }

    /**
     * WebViews for safe application. We create a webview, add javascript
     * and even run MathJax.
     *
     * @param key The string to be displayed on the screen.
     * @return A WebView to contain text
     */
    @SuppressLint("SetJavaScriptEnabled")
    private WebView generateWebViewForSafe(String key) {
        final WebView webView = new WebView(appContext);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollContainer(false);

        try {
            webViewCacheLock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        webView.loadDataWithBaseURL("http://safe_internal",
                "<span id='math'>" + doubleEscapeTeX(key) + "</span>"
                        + "<script type='text/x-mathjax-config'>"
                        + "MathJax.Hub.Config({ "
                        + "showMathMenu: false, "
                        + "jax: ['input/TeX','output/SVG'], "
                        + "extensions: ['tex2jax.js'], "
                        + "TeX: { extensions: ['AMSmath.js','AMSsymbols.js',"
                        + "'noErrors.js','noUndefined.js'] } "
                        + "});</script>"
                        + "<script type='text/javascript' "
                        + "src='file:///android_asset/MathJax/MathJax.js'"
                        + "></script>"
                        + "<script>MathJax.Hub.Queue(['Typeset', MathJax.Hub]);</script>",
                "text/html", "utf-8", "");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webViewCacheLock.release();
            }
        });
        return webView;
    }

    /**
     * Set the webView data to display the text and
     * then render MathJax inside it.
     *
     * @param webView WebView inside which data should be added
     * @param data    The data to be set on display
     * @return Return the webView object
     */
    private static WebView setWebViewData(final WebView webView, final String data) {
        webView.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.evaluateJavascript("document.getElementById('math').innerHTML='"
                        + doubleEscapeTeX(data) + "';"
                        + "MathJax.Hub.Queue(['Typeset',MathJax.Hub]);", null);
            }
        }, 100);
        return webView;
    }

    private static String doubleEscapeTeX(String str) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '\'') {
                builder.append('\\');
            }
            if (str.charAt(i) != '\n') {
                builder.append(str.charAt(i));
            }
            if (str.charAt(i) == '\\') {
                builder.append('\\');
            }
        }
        return builder.toString();
    }
}
