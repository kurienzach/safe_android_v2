package com.iitb.cse.arkenstone.safe.quiz;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Module;
import com.iitb.cse.arkenstone.safe.quiz.QuizData;
import com.iitb.cse.arkenstone.safe.quiz.services.QuizMonitorService;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;

import java.util.ArrayList;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

/**
 * Created by sonika on 31/10/16.
 */

public class QuizNavigation extends Activity {
    ListView listView ;

    //Communicate with QuizMonitor Service
    Messenger myService = null;
    boolean isBound = false;
    Message pendingMessage = null;
    private final ServiceConnection myConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            myService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, AppConstant.MSG_CONNECTED);
                myService.send(msg);
                if (pendingMessage != null) {
                    myService.send(pendingMessage);
                    pendingMessage = null;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            myService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);

        QuizData.setNavigationQuizStatus(2);

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.list);

        ArrayList<String> values = new ArrayList<String>();
        ArrayList<Module> questionModules = QuizData.getQuestionModules();
        for (int i = 0; i < questionModules.size(); i++) {
            Module curQuestionModule = questionModules.get(i);
            int questionLength = Math.min(30, curQuestionModule.questions.get(0).description.length()-4);
            values.add("Question "+ (i+1) + ": " + curQuestionModule.questions.get(0).description.substring(3, questionLength));
        }

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

//                // ListView Clicked item value
//                String  itemValue    = (String) listView.getItemAtPosition(position);
//
//                // Show Alert
//                Toast.makeText(getApplicationContext(),
//                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
//                        .show();
                QuizData.setNavigationQuizStatus(3);
                Intent intent=new Intent();
                intent.putExtra("QuestionNumber",position);
                setResult(1,intent);
                finish();//finishing activity

            }

        });

        //Start QuizMonitor service (but not bind until quiz starts)
        startService(new Intent(this, QuizMonitorService.class));
    }

    @Override
    protected void onDestroy() {
        QuizData.setNavigationQuizStatus(3);
        try {
            doUnbindService();
        } catch (Throwable t) {
            logError(logTag(), "Failed to unbind service");
            t.printStackTrace();
        }
        //Stop the monitor service
        stopService(new Intent(this, QuizMonitorService.class));
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        QuizData.setNavigationQuizStatus(3);
        try {
            doUnbindService();
        } catch (Throwable t) {
            logError(logTag(), "Failed to unbind service");
            t.printStackTrace();
        }
        //Stop the monitor service
        stopService(new Intent(this, QuizMonitorService.class));
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        logDebug(logTag(), "OnPause");
        super.onPause();

        if (QuizData.getNavigationQuizStatus() != 3) {

            //This could happen if screen goes off, so make sure of that
            if (!ContextHelper.isScreenOn()) {
                return;
            }

            //Notify Monitor that we lost focus not because screen was off!
            sendMessageToService(AppConstant.MSG_UI_OUT_OF_FOCUS);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        logDebug(logTag(), "OnWindowFocusChanged : " + hasFocus);
        super.onWindowFocusChanged(hasFocus);

        //We are in the middle of a quiz and dialogs are not open
        //logDebug(logTag(), String.valueOf(dialogOpen));
        if (QuizData.getNavigationQuizStatus() != 3) {

            //This could happen if screen goes off, so make sure of that
            if (!ContextHelper.isScreenOn()) {
                return;
            }

            //Notify monitor service
            if (hasFocus) {
                sendMessageToService(AppConstant.MSG_UI_IN_FOCUS);
            } else {
                sendMessageToService(AppConstant.MSG_UI_OUT_OF_FOCUS);
            }
        }
    }

    @Override
    protected void onResume() {
        logDebug(logTag(), "OnResume");

//        if (ContextHelper.logFloatingApps()) {
//            Toast.makeText(
//                    this,
//                    "Floating apps detected.. this will be notified to instructor",
//                    Toast.LENGTH_LONG).show();
//        }

        //if (quizState == QuizActivity.QuizState.STARTED) {
//            if (currentIndex < 0 || currentIndex > numQuestions - 1) {
//                currentIndex = 0;
//            }
//            //set the question
//            setQuestion(currentIndex, Mode.REPLACE);

            //if (currentIndex == 0) prevButton.setEnabled(false);
            //if (currentIndex + 1 == numQuestions) nextButton.setEnabled(false);

            //Notify monitor that we got focus back
            sendMessageToService(AppConstant.MSG_UI_IN_FOCUS);

            //Don't let lock-screen lock while quiz is progressing
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            //Dismiss keyguard not working in lollipop
        //}
        super.onResume();
    }

    /**
     * Connects to the monitor service for passing messages.
     */
    void connectToService() {
        if (QuizMonitorService.isRunning()) {
            doBindService();
        }
    }

    /**
     * Bind to service, called when connecting.
     */
    void doBindService() {
        bindService(new Intent(this, QuizMonitorService.class), myConnection, Context.BIND_AUTO_CREATE);
        isBound = true;
    }

    /**
     * unbinds the service.
     */
    void doUnbindService() {
        if (isBound) {
            // If we have received the service, and hence registered with it,
            // then now is the time to unregister.
            if (myService != null) {
                try {
                    Message msg = Message.obtain(null, AppConstant.MSG_DISCONNECTED);
                    myService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                    // We are trying to report it to the server
                    QuizLogging.newLog(new QuizLogging.QuizLog("Service has crashed!", null));
                }
            }
            // Detach our existing connection.
            unbindService(myConnection);
            isBound = false;
        }
    }

    /*Send message to monitor service*/
    private void sendMessageToService(int what) {
        if (!isBound) {
            connectToService();
            pendingMessage = Message.obtain(null, what);
        }
        if (isBound && myService != null) {
            try {
                Message msg = Message.obtain(null, what);
                myService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
                logError(logTag(), e);
            }
        }
    }
}
