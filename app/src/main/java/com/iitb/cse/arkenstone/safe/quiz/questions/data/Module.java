package com.iitb.cse.arkenstone.safe.quiz.questions.data;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import com.iitb.cse.arkenstone.safe.quiz.questions.QuizDeserializer;

import java.util.ArrayList;

/**
 * Class depicting a question module.
 */
public class Module {

    /**
     * Description of the module.
     */
    public String description = "";

    /**
     * Id for the module.
     */
    public Integer id = null;

    /**
     * The module number.
     */
    public transient String moduleNumber = "";

    /**
     * List of questions in a module.
     */
    public ArrayList<Question> questions = null;

    /**
     * After creating the objects, call this method to set the module number
     * along with the display of question numbers on the tabs.
     * This method also sets the module objects for the questions and then
     * call the `post_create` method.
     *
     * @param number    The number for the module.
     * @param tabLayout The tab layout for displaying the question numbers
     */
    public void setModuleNumber(int number, final TabLayout tabLayout) {
        moduleNumber = String.valueOf(number);

        if (questions.size() == 1) {
            questions.get(0).post_save();
            questions.get(0).module = this;
            questions.get(0).questionNumber = moduleNumber;
            tabLayout.post(new Runnable() {
                @Override
                public void run() {
                    tabLayout.addTab(tabLayout.newTab().setText(moduleNumber));
                }
            });
            return;
        }

        for (int i = 0; i < questions.size(); i++) {
            questions.get(i).post_save();
            questions.get(i).module = this;

            final char alphabet = (char) ((int) 'a' + i);
            questions.get(i).questionNumber = moduleNumber + alphabet;
            tabLayout.post(new Runnable() {
                @Override
                public void run() {
                    tabLayout.addTab(tabLayout.newTab().setText(moduleNumber + alphabet));
                }
            });
        }
    }

    /**
     * Return the Question fragment for a particular question number.
     *
     * @param questionNumber The question number of the question
     * @return The view for the question.
     */
    public Fragment getQuestionView(Integer questionNumber) {
        return QuizDeserializer.getChildFragment(getQuestion(questionNumber));
    }

    /**
     * Return the Question for a particular question number.
     *
     * @param questionNumber The question number of the question
     * @return The question.
     */
    public Question getQuestion(Integer questionNumber) {
        Integer index = questionNumber - Integer.parseInt(moduleNumber);

        if (index >= questions.size()) {
            logError(logTag(), "Requested question with a question number where the question"
                    + "number is not a part of this module");
            return null;
        }

        return questions.get(index);
    }

}
