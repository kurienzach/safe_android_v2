package com.iitb.cse.arkenstone.safe.quiz;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.iitb.cse.arkenstone.safe.quiz.questions.QuizDeserializer;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Module;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

/**
 * Singleton class to hold data about the current
 * running quiz.
 */
public class QuizData {

    /**
     * List of all question modules for the current quiz.
     */
    private static ArrayList<Module> questionModules;

    /**
     * UUID of the quiz.
     */
    private static String QuizUuid;

    /**
     * Pager object for the current quiz.
     */
    private static QuizPagerAdapter quizPagerAdapter;

    /**
     * Quiz Activity instance.
     */
    private static QuizActivity quizActivity;

    /**
     * Straddle duration for the quiz in seconds.
     */
    private static Integer straddleDuration;

    /**
     * True if the current quiz is in frozen state. Meaning,
     * it is not allowed to modify the answers.
     */
    private static boolean quizFrozen;

    private static int navigationQuizStatus = 0;

    public static ArrayList<Module> getQuestionModules() {
        return questionModules;
    }

    /**
     * Given the map of json response from server for fetch,
     * extract and parse the questions.
     *
     * @param jsonResponse The json response object extracted from the server response
     */
    public static void parseQuestionsJson(Map<String, Object> jsonResponse) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Question.class, new QuizDeserializer())
                .create();
        Type type = new TypeToken<ArrayList<Module>>() {
        }
                .getType();

        questionModules = gson.fromJson(gson.toJson(jsonResponse.get("question_modules")), type);
        LoadingScreen.getInstance().addDebugInfo("Questions data parse complete");
        LoadingScreen.getInstance()
                .addDebugInfo("Number of question modules : " + questionModules.size());
    }

    public static String getQuizUuid() {
        return QuizUuid;
    }

    public static void setQuizUuid(String quizUuid) {
        QuizUuid = quizUuid;
    }

    public static void setQuizActivity(QuizActivity quizActivity) {
        QuizData.quizActivity = quizActivity;
    }

    public static void setQuizPagerAdapter(QuizPagerAdapter quizPagerAdapter) {
        QuizData.quizPagerAdapter = quizPagerAdapter;
    }

    public static QuizActivity getQuizActivity() {
        return quizActivity;
    }

    public static QuizPagerAdapter getQuizPagerAdapter() {
        return QuizData.quizPagerAdapter;
    }

    /**
     * Get the straddle duration for the quiz.
     *
     * @return The duration in seconds.
     */
    public static Integer getStraddleDuration() {
        return straddleDuration;
    }

    public static void setStraddleDuration(Integer straddleDuration) {
        QuizData.straddleDuration = straddleDuration;
    }

    public static boolean isQuizFrozen() {
        return quizFrozen;
    }

    public static void setQuizFrozen(boolean quizFrozen) {
        QuizData.quizFrozen = quizFrozen;
    }

    public static int getNavigationQuizStatus() { return navigationQuizStatus; }

    public static void setNavigationQuizStatus(int status) { navigationQuizStatus = status; logDebug(logTag(), "navigation status set to: " + String.valueOf(navigationQuizStatus));}
}
