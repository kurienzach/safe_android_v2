/**
 * This package contains the classes used for questions and answers
 * that are displayed as fragments in the main quiz window.
 */

package com.iitb.cse.arkenstone.safe.quiz.questions;
