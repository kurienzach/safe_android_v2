package com.iitb.cse.arkenstone.safe.utils.listeners;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.iitb.cse.arkenstone.safe.LoginActivity;
import com.iitb.cse.arkenstone.safe.network.SafeWebSocket;
import com.iitb.cse.arkenstone.safe.utils.NetworkUtils;

/**
 * Listener to changes in Connectivity like wifi, bluetooth, etc.
 */
public class ConnectivityListener extends BroadcastReceiver {

    @Override
    public final void onReceive(final Context context, final Intent intent) {
        ConnectivityManager manager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        String action = intent.getAction();


        //check whether intent is due to bluetooth
        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                    == BluetoothAdapter.STATE_OFF) {
                logDebug(logTag(), "Bluetooth off");
                // no need to log if bluetooth is off as we
                // require that bluetooth should be off
            } else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                    == BluetoothAdapter.STATE_ON) {
                logDebug(logTag(), "Bluetooth on");
                NetworkUtils.logConnectivityChange("Blutooth is switched on",
                        NetworkUtils.ActiveNetwork.BLUETOOTH);
            }
        }

        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
            // intent is either due to wifi or mobile
            // from api version greater than 23 getNetworkInfo is deprecated
            // broadcast is triggered only when there is a change in
            // current active network
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();

            // broadcast is received multiple times with identical intents
            // so compare current active network with previous active network
            // to check whether it is a duplicate broadcast so that identical
            // logs are not sent to server
            // Object comparison is not working, converted to string and compare
            String s1 = String.valueOf(networkInfo);
            String s2 = String.valueOf(
                    NetworkUtils.getCurrentActiveNetworkInfo());
            if (s1.equals(s2)) {
                return;
            }

            if (networkInfo == null) {
                // no network connectivity
                logDebug(logTag(), "No Network");
                NetworkUtils.setCurrentActiveNetworkInfo(null);
                NetworkUtils.logConnectivityChange("No Network",
                        NetworkUtils.ActiveNetwork.OTHER);
            } else {

                // We have network. Try to reload org list
                // if it is active.
                if (LoginActivity.isRunning()) {
                    LoginActivity.reloadOrgList();
                }

                if (SafeWebSocket.getInstance() != null) {
                    SafeWebSocket.getInstance().run();
                }

                // new active network found. so set it as current network
                NetworkUtils.setCurrentActiveNetworkInfo(networkInfo);

                if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI
                        && networkInfo.isConnected()) {
                    // on wifi
                    logDebug(logTag(), "wifi on");
                    NetworkUtils.logConnectivityChange("On Wifi Network",
                            NetworkUtils.ActiveNetwork.WIFI);
                } else if (networkInfo.getType()
                        == ConnectivityManager.TYPE_MOBILE
                        && networkInfo.isConnected()) {
                    // on mobile
                    logDebug(logTag(), "mobile data on");
                    NetworkUtils.logConnectivityChange("On Mobile Data Network",
                            NetworkUtils.ActiveNetwork.MOBILE);
                } else {
                    logDebug(logTag(), "Unknown network");
                    NetworkUtils.logConnectivityChange("Unknown network.",
                            NetworkUtils.ActiveNetwork.OTHER);
                }
            }
        }
    }
}
