package com.iitb.cse.arkenstone.safe.utils;

/**
 * This is used to track time of execution of a task.
 * This cannot be a singleton as it leads to data races.
 */
public class TimeUtil {
    /**
     * Start time of the timer.
     */
    private long startTime;

    /**
     * End time of the timer.
     */
    private long endTime;

    /**
     * This method is used to record the start time of an task .
     */
    public final void setStartTime() {
        startTime = System.currentTimeMillis();
    }

    /**
     * This method is used to record the finish time of an task.
     */
    public final void setEndTime() {
        endTime = System.currentTimeMillis();
    }

    /**
     * This method is used to obtain the total time in
     * milliseconds taken for a task.
     * @return duration in seconds.
     */
    public final long getDuration() {
        return endTime - startTime;
    }
}
