package com.iitb.cse.arkenstone.safe.quiz;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.iitb.cse.arkenstone.safe.BuildConfig;
import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.network.SafeWebSocket;
import com.iitb.cse.arkenstone.safe.quiz.services.QuizCountdownService;
import com.iitb.cse.arkenstone.safe.quiz.services.QuizMonitorService;
import com.iitb.cse.arkenstone.safe.quiz.QuizData;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.widget.WebViewCache;

import java.util.Locale;

public class QuizActivity extends AppCompatActivity {

    public boolean dialogOpen = false; //If exit/submit dialogs are open
    /**
     * The LoadingScreen instance for the QuizActivity.
     */
    LoadingScreen loadingScreen;
    /**
     * The QuizLoader instance for the QuizActivity.
     */
    QuizLoader quizLoader;
    //Communicate with QuizMonitor Service
    Messenger myService = null;
    boolean isBound = false;
    /**
     * The broadcast receiver for the quiz countdown. On
     * receiving the intent, we extract the time remaining.
     * If the time remaining is negative, it means the quiz
     * is infinite and if it is zero, it means the quiz is
     * complete. The display on the Bottom Navigation is set
     * appropriately.
     *
     * @see QuizCountdownService
     * @see BottomNavigation#setTimeDisplay(String)
     */
    BroadcastReceiver quizCountdownReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long remainingMillis = intent.getLongExtra("countdown", -2);

            // If > 0 : quiz counting down,
            // = 0 : quiz ended
            // < 0 : infinite quiz
            if (remainingMillis > 0) {
                long remainingMins = remainingMillis / 1000 / 60;
                long remainingSecs = (remainingMillis / 1000) % 60;
                String displayString = "%02d:%02d";
                BottomNavigation.instance.setTimeDisplay(String.format(
                        Locale.ENGLISH,
                        displayString,
                        remainingMins,
                        remainingSecs
                ));
            } else if (remainingMillis < 0) {
                BottomNavigation.instance.setTimeDisplay("--:--");
            } else {
                // Auto Submit quiz
                ContextHelper.hideSoftKeyboard(QuizActivity.this);
                LoadingScreen.getInstance().setCountDownDisplay("Auto Submitting");
                LoadingScreen.getInstance().showLoadingScreen();

                Integer straddleTime = ((Double) (Math.random()
                        * QuizData.getStraddleDuration())).intValue();
                LoadingScreen.getInstance().addDebugInfo("Straddle duration: "
                        + straddleTime + " seconds");
                // Start the submission after straddle duration. The duration
                // is in seconds. So, multiply with 1000
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doUnbindService();
                        QuizSubmissions.instance.submitQuiz();
                    }
                }, straddleTime * 1000);
            }
        }
    };
    //Maintain current state of the quiz. See the QuizState enum declared
    private QuizState quizState = QuizState.NOT_STARTED;
    private Message pendingMessage = null;
    private final ServiceConnection myConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            myService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, AppConstant.MSG_CONNECTED);
                myService.send(msg);
                if (pendingMessage != null) {
                    myService.send(pendingMessage);
                    pendingMessage = null;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            myService = null;
        }
    };

    public QuizState getQuizState() {
        return quizState;
    }

    public void setQuizStateToStarted() {
        quizState = QuizState.STARTED;
    }

    public void setQuizStateToNotStarted() {
        quizState = QuizState.NOT_STARTED;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        QuizData.setQuizUuid(getIntent().getStringExtra("quizKey"));
        QuizData.setQuizActivity(this);
        QuizData.setQuizFrozen(false);

        LoadingScreen.createInstance(this);
        QuizLoader.createInstance(this, QuizData.getQuizUuid());

        loadingScreen = LoadingScreen.getInstance();
        quizLoader = QuizLoader.getInstance();

        if (BuildConfig.DEBUG) {
            loadingScreen.displayDebugInfo(true);
        }

        quizLoader.fetchQuiz();

        try {
            WebViewCache.webViewCacheLock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            logError(logTag(), "Thread interrupted");
        }

        try {
            WebViewCache.webViewCacheCreate.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                LoadingScreen.getInstance().addDebugInfo("Create webView cache");
                AppData.setWebViewCache(new WebViewCache(QuizActivity.this));
                LoadingScreen.getInstance().addDebugInfo("WebView cache ready");
                WebViewCache.webViewCacheCreate.release();
                WebViewCache.webViewCacheLock.release();
            }
        });

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                LoadingScreen.getInstance().addDebugInfo("Creating webSockets");
                SafeWebSocket.createInstance(QuizData.getQuizUuid(), AppData.getLoginToken());
                LoadingScreen.getInstance().addDebugInfo("WebSockets Created");
                LoadingScreen.getInstance().addDebugInfo("Trying to connect web socket");
                SafeWebSocket.getInstance().run();
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        BottomNavigation.instance.setupBottomBar(this);
        QuizFab.instance.setupQuizFab(this);
        //quizState = QuizState.STARTED;

        registerReceiver(quizCountdownReceiver,
                new IntentFilter(QuizCountdownService.COUNTDOWN_BR));

        //Start QuizMonitor service (but not bind until quiz starts)
        startService(new Intent(this, QuizMonitorService.class));
    }

    @Override
    protected void onDestroy() {
        try {
            doUnbindService();
        } catch (Throwable t) {
            logError(logTag(), "Failed to unbind service");
            t.printStackTrace();
        }
        //Stop the monitor service
        stopService(new Intent(this, QuizMonitorService.class));
        unregisterReceiver(quizCountdownReceiver);
        logDebug(logTag(), "Unregistered quiz countdown receiver");
        stopService(new Intent(this, QuizCountdownService.class));
        logDebug(logTag(), "Stopped Quiz Countdown Service");
        QuizData.setQuizActivity(null);
        quizState = QuizState.NOT_STARTED;
        logDebug(logTag(), "OnDestroy");
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // Do nothing. Ignore the back button call
    }

    @Override
    protected void onPause() {
        logDebug(logTag(), "OnPause");
        super.onPause();

        if (quizState == QuizState.STARTED && QuizData.getNavigationQuizStatus() != 1) {

            //This could happen if screen goes off, so make sure of that
            if (!ContextHelper.isScreenOn()) {
                return;
            }

            //Notify Monitor that we lost focus not because screen was off!
            sendMessageToService(AppConstant.MSG_UI_OUT_OF_FOCUS);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        logDebug(logTag(), "OnWindowFocusChanged : " + hasFocus);
        super.onWindowFocusChanged(hasFocus);

        //We are in the middle of a quiz and dialogs are not open
        logDebug(logTag(), String.valueOf(dialogOpen));
        if (quizState == QuizState.STARTED && !dialogOpen && QuizData.getNavigationQuizStatus() != 1) {

            //This could happen if screen goes off, so make sure of that
            if (!ContextHelper.isScreenOn()) {
                return;
            }

            //Notify monitor service
            if (hasFocus) {
                sendMessageToService(AppConstant.MSG_UI_IN_FOCUS);
            } else {
                sendMessageToService(AppConstant.MSG_UI_OUT_OF_FOCUS);
            }
        }

        //This flag will be true if focus change was triggered by exit/submit dialog
        //Make it false anyway
        dialogOpen = false;
    }

    @Override
    protected void onResume() {
        logDebug(logTag(), "OnResume");

        if (ContextHelper.logFloatingApps()) {
            Toast.makeText(
                    this,
                    "Floating apps detected.. this will be notified to instructor",
                    Toast.LENGTH_LONG).show();
        }

        if (quizState == QuizState.STARTED) {
//            if (currentIndex < 0 || currentIndex > numQuestions - 1) {
//                currentIndex = 0;
//            }
//            //set the question
//            setQuestion(currentIndex, Mode.REPLACE);

            //if (currentIndex == 0) prevButton.setEnabled(false);
            //if (currentIndex + 1 == numQuestions) nextButton.setEnabled(false);

            //Notify monitor that we got focus back
            sendMessageToService(AppConstant.MSG_UI_IN_FOCUS);

            //Don't let lock-screen lock while quiz is progressing
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            //Dismiss keyguard not working in lollipop
        }
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1) {
            if (resultCode == 1) {
                int QuestionNumber = data.getIntExtra("QuestionNumber", -1);
                logDebug(logTag(), "Question Number is : " + QuestionNumber);
                if (QuestionNumber != -1) quizLoader.changePage(QuestionNumber);

            }
        }
    }

    /**
     * Connects to the monitor service for passing messages.
     */
    void connectToService() {
        if (QuizMonitorService.isRunning()) {
            doBindService();
        }
    }

    /**
     * Bind to service, called when connecting.
     */
    void doBindService() {
        bindService(new Intent(this, QuizMonitorService.class), myConnection,
                Context.BIND_AUTO_CREATE);
        isBound = true;
    }

    /**
     * unbinds the service.
     */
    void doUnbindService() {
        if (isBound) {
            // If we have received the service, and hence registered with it,
            // then now is the time to unregister.
            if (myService != null) {
                try {
                    Message msg = Message.obtain(null, AppConstant.MSG_DISCONNECTED);
                    myService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                    // We are trying to report it to the server
                    QuizLogging.newLog(new QuizLogging.QuizLog("Service has crashed!", null));
                }
            }
            // Detach our existing connection.
            unbindService(myConnection);
            isBound = false;
        }
    }

    /*Send message to monitor service*/
    private void sendMessageToService(int what) {
        if (!isBound) {
            connectToService();
            pendingMessage = Message.obtain(null, what);
        }
        if (isBound && myService != null) {
            try {
                Message msg = Message.obtain(null, what);
                myService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
                logError(logTag(), e);
            }
        }
    }

    /*Maintaining quiz state*/
    public enum QuizState {
        NOT_STARTED, STARTED, SUBMITTED, EXITED
    }
}
