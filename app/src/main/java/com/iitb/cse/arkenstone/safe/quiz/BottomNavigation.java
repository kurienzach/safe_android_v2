package com.iitb.cse.arkenstone.safe.quiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.quiz.QuizData;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Bottom Navigation bar.
 */
public final class BottomNavigation implements AHBottomNavigation.OnTabSelectedListener {

    static BottomNavigation instance = new BottomNavigation();
    /**
     * Bottom Navigation.
     */
    AHBottomNavigation bottomNavigation;
    // Navigation items on the screen.
    AHBottomNavigationItem navigationBtn;
    AHBottomNavigationItem time;
    AHBottomNavigationItem exitBtn;
    AHBottomNavigationItem submitBtn;
    QuizActivity quizActivity;
    // Toggle for turning time display ON/OFF
    private Boolean timeDisplay = true;

    /**
     * Private constructor to defeat instantiation.
     */
    private BottomNavigation() {
        // No body needed
    }

    /**
     * Setup the bottom navigation bar for the quiz ui.
     *
     * @param activity Activity in which the bottom bar is displayed.
     */
    @SuppressWarnings("deprecation")
    public void setupBottomBar(final QuizActivity activity) {

        quizActivity = activity;
        bottomNavigation =
                (AHBottomNavigation) activity.findViewById(R.id.bottom_navigation);


        // Setup the menu items.
        navigationBtn = new AHBottomNavigationItem(
                "Nav",
                R.drawable.ic_view_headline_black_24px,
                ContextCompat.getColor(activity, R.color.colorPrimary));
        time = new AHBottomNavigationItem(
                "Time",
                R.drawable.ic_query_builder_black_24px,
                ContextCompat.getColor(activity, R.color.colorPrimary));
        exitBtn = new AHBottomNavigationItem(
                "Exit",
                R.drawable.ic_exit_to_app_white_24dp,
                ContextCompat.getColor(activity, R.color.colorPrimary));
        submitBtn = new AHBottomNavigationItem(
                "Submit",
                R.drawable.ic_done_black_24px,
                ContextCompat.getColor(activity, R.color.colorPrimary));

        bottomNavigation.addItem(time);
        bottomNavigation.addItem(navigationBtn);
        bottomNavigation.addItem(submitBtn);
        bottomNavigation.addItem(exitBtn);
        bottomNavigation.setDefaultBackgroundColor(
                ContextCompat.getColor(activity, R.color.colorPrimary));
        bottomNavigation.setForceTint(true);
        bottomNavigation.setColored(true);

        // The layout needs a bottom padding because the navigation takes
        // up some space. There may be some way in xml to fix this. But
        // this also works.
        LinearLayout quizUiLayout = (LinearLayout) activity.findViewById(R.id.quiz_ui);
        bottomNavigation.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        quizUiLayout.setPadding(
                quizUiLayout.getPaddingLeft(),
                quizUiLayout.getPaddingTop(),
                quizUiLayout.getPaddingRight(),
                quizUiLayout.getPaddingBottom() + bottomNavigation.getMeasuredHeight()
        );

        try {
            Field field = AHBottomNavigation.class.getDeclaredField("views");
            field.setAccessible(true);
            @SuppressWarnings("unchecked")
            ArrayList<View> views = (ArrayList<View>) field.get(bottomNavigation);
            for (View view : views) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        bottomNavigation.setOnTabSelectedListener(this);
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {

        // On submit button click
        if (position == 2) {
            quizActivity.dialogOpen = true;
            new AlertDialog.Builder(quizActivity)
                    .setTitle("Submit")
                    .setMessage("Are you sure you want to submit?")
                    .setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int id) {
                                    quizActivity.dialogOpen = true;
                                    quizActivity.doUnbindService();
                                    LoadingScreen.getInstance().setCountDownDisplay("Submitting");
                                    LoadingScreen.getInstance().showLoadingScreen();
                                    QuizSubmissions.instance.submitQuiz();
                                }
                            }
                    )
                    .setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int id) {
                                    quizActivity.dialogOpen = false;
                                    dialogInterface.cancel();
                                }
                            }
                    ).show();
        }

        // On timer click
        if (position == 0) {
            timeDisplay = !timeDisplay;
        }

        // On exit button click
        if (position == 3) {
            quizActivity.dialogOpen = true;
            new AlertDialog.Builder(quizActivity)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to exit? Your responses will not be submitted and the instructor will be notified.")
                    .setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int id) {
                                    quizActivity.dialogOpen = true;
                                    quizActivity.doUnbindService();
                                    LoadingScreen.getInstance().setCountDownDisplay("Exiting");
                                    LoadingScreen.getInstance().showLoadingScreen();
                                    //send to server
                                    QuizLogging.newLog(new QuizLogging.QuizLog(
                                            "Quiz exited explicitly!",
                                            QuizLogging.QuizLog.Severity.CRITICAL));
                                    AppData.setLoginToken(null);
                                    AppData.setQuizStarted(false);
                                    QuizData.getQuizActivity().setQuizStateToNotStarted();
                                    QuizData.getQuizActivity().finish();
                                }
                            }
                    )
                    .setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int id) {
                                    quizActivity.dialogOpen = false;
                                    dialogInterface.cancel();
                                }
                            }
                    ).show();
        }

        // On nav button click
        if (position == 1) {
            QuizData.setNavigationQuizStatus(1);
            Intent myIntent = new
                    Intent(quizActivity.getApplicationContext(), QuizNavigation.class);
            quizActivity.startActivityForResult(myIntent, 1);
        }
        return false;
    }

    /**
     * Set the text for the time in the bottom navigation bar.
     *
     * @param text The text to be displayed
     */
    public void setTimeDisplay(final String text) {
        bottomNavigation.post(new Runnable() {
            @Override
            public void run() {
                if (timeDisplay) {
                    time.setTitle(text);
                    bottomNavigation.refresh();
                } else {
                    time.setTitle("");
                    bottomNavigation.refresh();
                }
            }
        });
    }
}
