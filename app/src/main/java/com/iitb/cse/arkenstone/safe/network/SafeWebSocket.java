package com.iitb.cse.arkenstone.safe.network;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logWarning;

import com.iitb.cse.arkenstone.safe.quiz.LoadingScreen;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.ws.WebSocket;
import okhttp3.ws.WebSocketCall;
import okhttp3.ws.WebSocketListener;
import okio.Buffer;
import okio.ByteString;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Class that is used for managing WebSockets for quiz.
 */
public final class SafeWebSocket {

    /**
     * The builder used to create the web socket.
     */
    OkHttpClient.Builder clientBuilder;

    /**
     * Request object for the web socket.
     */
    Request request;

    /**
     * The web socket listener which contains the
     * functions for the web socket.
     */
    WebSocketListener socketListener;

    /**
     * Web socket call provided for cancelling and
     * restarting the web sockets.
     */
    WebSocketCall wsCall;

    /**
     * Reference to the webSocket for the session.
     */
    private WebSocket webSocketRef;

    /**
     * Web Socket singleton instance.
     */
    private static SafeWebSocket instance;

    /**
     * Get the singleton instance.
     *
     * @param quizUuid Quiz UUID as entered by the user.
     * @param token    Authentication Token for the quiz.
     */
    public static void createInstance(String quizUuid, String token) {
        instance = new SafeWebSocket(quizUuid, token);
    }

    /**
     * Return the singleton instance.
     *
     * @return The WebSocket object.
     */
    public static SafeWebSocket getInstance() {
        return instance;
    }

    /**
     * Create the web socket object with the
     * given UUID and user login token.
     *
     * @param quizUuid Quiz UUID as entered by the user.
     * @param token    Authentication Token for the quiz.
     */
    private SafeWebSocket(String quizUuid, String token) {

        String url = AppConstant.getRootUrl() + AppConstant.WEB_SOCKET_URL;
        url = String.format(Locale.ENGLISH, url, quizUuid, token);

        clientBuilder = ServerInterface.getHttpClientBuilder(AppConstant.getAcceptCertificates());
        clientBuilder.readTimeout(0, TimeUnit.MILLISECONDS)
                .writeTimeout(0, TimeUnit.MILLISECONDS)
                .connectTimeout(0, TimeUnit.MILLISECONDS);

        request = new Request.Builder()
                .get()
                .url(url)
                .build();

        setupSocketListener();
    }

    /**
     * Create the socket connection to the server and
     * start the communication.
     */
    public void run() {
        if (wsCall != null) {
            logError(logTag(), "WebSocket is not null: Reconnecting to server");
        }
        wsCall = WebSocketCall.create(clientBuilder.build(), request);
        wsCall.enqueue(socketListener);
    }

    /**
     * Setup the socket listener object.
     */
    private void setupSocketListener() {
        socketListener = new WebSocketListener() {
            @Override
            public void onOpen(okhttp3.ws.WebSocket webSocket, Response response) {
                webSocketRef = webSocket;
                logDebug(logTag(), "WebSocket connection successful");
                LoadingScreen.getInstance().addDebugInfo("WebSocket connection successful");
            }

            @Override
            public void onFailure(IOException exp, Response response) {
                logWarning(logTag(), "WebSocket connection failed");
                LoadingScreen.getInstance().addDebugInfo("WebSocket connection failed");
                webSocketRef = null;
            }

            @Override
            public void onMessage(ResponseBody message) throws IOException {
                logDebug(logTag(), "Received in web socket: " + message.string());
                message.close();
                // Do actions after parsing them
            }

            @Override
            public void onPong(Buffer payload) {
                payload.write(ByteString.encodeUtf8("a"));
            }

            @Override
            public void onClose(int code, String reason) {
                logDebug(logTag(), "WebSocket closed!");
                LoadingScreen.getInstance().addDebugInfo("WebSocket connection closed");
                webSocketRef = null;
            }
        };
    }

    /**
     * Push data with JSON format. Must be called in
     * a background thread.
     *
     * @param content The JSON content to send to the server.
     * @return True if successfully pushed. False otherwise.
     */
    public boolean pushToServer(String content) {
        if (webSocketRef == null) {
            return false;
        } else {
            try {
                webSocketRef.sendMessage(
                        RequestBody.create(ServerInterface.WEB_SOCKET_TEXT, content));
            } catch (IOException exp) {
                exp.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
