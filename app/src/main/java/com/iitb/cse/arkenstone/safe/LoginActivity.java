package com.iitb.cse.arkenstone.safe;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.NetworkUtils;
import com.iitb.cse.arkenstone.safe.utils.PermissionUtils;
import com.iitb.cse.arkenstone.safe.utils.TimeUtil;
import com.iitb.cse.arkenstone.safe.utils.widget.SafeSpinner;

import okhttp3.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends Activity {

    /**
     * Keep track of the current activity object.
     * This is a volatile object to keep it thread safe.
     */
    private static volatile LoginActivity instance = null;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask authTask = null;

    /**
     * Keep track of the task to load org list so that we can
     * cancel if needed.
     */
    private RetrieveOrgListTask orgListTask = null;

    /**
     * A variable that tracks whether the org list should be reloaded
     * or not. This is useful when url is changed and we need to run
     * the async task again.
     */
    private static Boolean shouldReloadOrgList = false;

    /**
     * Saved organization in sharedPrefs.
     */
    private String savedOrg;

    /**
     * The field to enter the organization.
     */
    private SafeSpinner orgViewSpinner;

    /**
     * Spinner adapter for organization.
     */
    private ArrayAdapter<String> orgViewAdapter;

    /**
     * Data for organization.
     * Format : id -> name
     */
    private Map<String, String> organizationList;

    /**
     * The field to enter the username.
     */
    private EditText usernameView;

    /**
     * The field to enter the password.
     */
    private EditText passwordView;

    /**
     * Save password or not.
     */
    private Boolean savePassword = false;

    /**
     * Login form scroll view.
     */
    private View loginFormView;

    /**
     * Progress Bar view.
     */
    private View progressView;

    /**
     * Snackbar to show no network connection.
     */
    private Snackbar noNetConnSnackBar;

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextHelper.isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        setContentView(R.layout.activity_login);

        ContextHelper.setupHideKeyboardOnOutsideTouch(
                findViewById(R.id.login_activity), this);

        orgViewSpinner = (SafeSpinner) findViewById(R.id.organization_spinner);
        usernameView = (EditText) findViewById(R.id.userid);
        passwordView = (EditText) findViewById(R.id.password);

        passwordView.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(final TextView textView,
                                                  final int id,
                                                  final KeyEvent keyEvent) {
                        if (id == R.id.login || id == EditorInfo.IME_NULL) {
                            attemptLogin();
                            return true;
                        }
                        return false;
                    }
                });

        CheckBox savePassBox = (CheckBox) findViewById(R.id.save_password);
        savePassBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(final CompoundButton btn,
                                                 final boolean isChecked) {
                        savePassword = isChecked;
                    }
                });

        Button signInButton = (Button) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                attemptLogin();
            }
        });
        signInButton.getBackground().setColorFilter(
                Color.parseColor("#4169E1"), PorterDuff.Mode.MULTIPLY
        );
        signInButton.setTextColor(Color.WHITE);

        loginFormView = findViewById(R.id.email_login_form);
        progressView = findViewById(R.id.login_progress);

        orgViewAdapter = SafeSpinner.createSpinnerAdapter(this);
        organizationList = new Hashtable<>();
        orgViewSpinner.setAdapter(orgViewAdapter);

        // Populate default values because when user never opened
        // preferences then they will be none. This will read settings.xml
        // file and set the default values defined there. Setting the
        // readAgain argument to false means this will only set the default
        // values if this method has never been called in the past
        // so you don't need to worry about overriding the user's
        // settings each time your Activity is created.

        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        ServerInterface.reloadServerBaseUrl(this);

        loadOrganizationList();

        Button settingsButton = (Button) findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent myIntent = new
                        Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(myIntent);
            }
        });

        // Populate saved values from shared prefs.
        final SharedPreferences sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(this);

        savedOrg = sharedPrefs.getString(AppConstant.LOGIN_ORG, "");
        String savedUsername =
                sharedPrefs.getString(AppConstant.LOGIN_USERNAME, "");
        String savedPassword =
                sharedPrefs.getString(AppConstant.LOGIN_PASSWORD, "");

        if (!"".equals(savedUsername)) {
            usernameView.setText(savedUsername);
            passwordView.requestFocus();

            if (!"".equals(savedPassword)) {
                passwordView.setText(savedPassword);
                savePassBox.setChecked(true);

                signInButton.setFocusable(true);
                signInButton.requestFocus();
            }
        }

        // currently we are not providing user option to not show instructions
        // so below checks always true
        if (sharedPrefs.getBoolean(AppConstant.PREF_SHOW_INSTRUCTIONS, true)) {
            ContextHelper.showInstructions(this);
        }

        if (ContextHelper.logFloatingApps()) {
            Snackbar.make(
                    findViewById(R.id.login_activity),
                    R.string.floating_apps_detected,
                    Snackbar.LENGTH_LONG).show();
        }

        // bluetooth checking here if necessary.
    }

    @Override
    protected final void onResume() {
        super.onResume();

        if (shouldReloadOrgList) {
            loadOrganizationList();
        }
    }

    @Override
    protected final void onStart() {
        super.onStart();

        synchronized (LoginActivity.class) {
            LoginActivity.instance = this;
        }
    }

    @Override
    protected final void onStop() {
        super.onStop();

        synchronized (LoginActivity.class) {
            LoginActivity.instance = null;
        }
    }

    @Override
    protected final void onActivityResult(final int requestCode,
                                          final int resultCode,
                                          final Intent data) {

        if (requestCode == AppConstant.REQUEST_PERMISSION_SETTING) {
            PermissionUtils.checkAndRequestPermission(this);
        } else if (requestCode == AppConstant.REQUEST_CODE_OVERLAY_PERMISSION) {
            //if permission is already granted
            // then handleSystemOverlayPermission will simply return
            PermissionUtils.handleSystemOverlayPermission(this);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public final void onRequestPermissionsResult(
            final int requestCode,
            @NonNull final String[] permissions,
            @NonNull final int[] grantResults) {
        if (requestCode == AppConstant.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Boolean permissionDenied = false;

            for (int i = 0; i < permissions.length; i++) {

                String permission = permissions[i];

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionDenied = true;
                    Boolean showRationale = ActivityCompat
                            .shouldShowRequestPermissionRationale(
                                    this,
                                    permission);

                    if (showRationale) {
                        // user denied permission without
                        // checking "never ask again" we need to show dialog
                        // explaining why he need to accept the permission
                        // when he clicks ok we need to ask permission again
                        PermissionUtils.handlePermissionDenied(this);
                    } else {
                        // user denied permission with never show again flag
                        // first need to show dialog explaining why he need
                        // to accept the permission when he clicks ok we
                        // need to take user to app settings
                        PermissionUtils.handleNeverShowAgain(this);
                    }
                }
            }
            // all permissions are granted
            // need to ask for system overlay permission
            if (!permissionDenied) {
                PermissionUtils.handleSystemOverlayPermission(this);
            }
        }
        super.onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults);
    }

    /**
     * Is the current activity running?
     *
     * @return True if running, false otherwise.
     */
    public static Boolean isRunning() {
        return instance != null;
    }


    /**
     * Load org list to display on the screen.
     */
    private void loadOrganizationList() {

        if (orgListTask != null) {
            return;
        }

        if (noNetConnSnackBar != null && noNetConnSnackBar.isShown()) {
            noNetConnSnackBar.dismiss();
        }

        // Hide keyboard when clicked on some button
        InputMethodManager imm = (InputMethodManager)
                this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(passwordView.getWindowToken(), 0);

        if (!NetworkUtils.isNetworkAvailable()) {

            noNetConnSnackBar = Snackbar
                    .make(findViewById(R.id.login_activity),
                            "No network connection!",
                            Snackbar.LENGTH_INDEFINITE);

            noNetConnSnackBar.setAction("Reload", new View.OnClickListener() {
                @Override
                public final void onClick(View view) {
                    noNetConnSnackBar.dismiss();
                    reloadOrgList();
                }
            });

            noNetConnSnackBar.show();

            return;
        }

        if (!ContextHelper.isEmulator() && !NetworkUtils.externalConnectionOff(this, false)) {
            // A toast will be shown by the method
            return;
        }

        showProgress(true);

        orgListTask = new RetrieveOrgListTask();
        orgListTask.execute((Void) null); // Perform list retrieval async
    }

    /**
     * Reload the organization list. This just sets the shouldReloadOrgList
     * variable to true and onResume takes care of the rest.
     */
    public static void reloadOrgList() {

        synchronized (LoginActivity.class) {
            if (instance == null) {
                shouldReloadOrgList = true;
            } else {
                instance.loadOrganizationList();
            }
        }
    }

    /**
     * Task to attempt the Login.
     */
    private void attemptLogin() {

        if (authTask != null) {
            return;
        }

        // Hide keyboard when clicked on some button
        InputMethodManager imm = (InputMethodManager)
                this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(passwordView.getWindowToken(), 0);

        if (!NetworkUtils.isNetworkAvailable()) {

            Snackbar.make(
                    findViewById(R.id.login_activity),
                    "No network connection!",
                    Snackbar.LENGTH_LONG
            ).show();

            return;
        }

        if (!ContextHelper.isEmulator() && !NetworkUtils.externalConnectionOff(this, false)) {
            // A toast will be shown by the method
            return;
        }

        // Reset errors
        usernameView.setError(null);
        passwordView.setError(null);

        // Store values at the time of login attempt

        String org;
        String orgId;

        try {
            org = orgViewSpinner.getText().toString();
            orgId = organizationList.get(org);
        } catch (ArrayIndexOutOfBoundsException e) {
            Snackbar.make(
                    findViewById(R.id.login_activity),
                    "No Organization Selected!",
                    Snackbar.LENGTH_LONG).show();
            return;
        }

        String userName = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        Boolean cancel = false;
        View focusView = null;

        if (password.isEmpty()) {
            passwordView.setError(getString(R.string.error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }

        if (userName.isEmpty()) {
            usernameView.setError(getString(R.string.error_field_required));
            focusView = usernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            authTask = new UserLoginTask(org, orgId, userName, password);
            authTask.execute((Void) null); // Perform Login asynchronously
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show Show or hide the progress UI.
     */
    public final void showProgress(final Boolean show) {

        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs,
        // which allow for very easy animations. If available,
        // use these APIs to fade-in the progress spinner.
        int shortAnimTime = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // Setup Login Form View
        if (show) {
            loginFormView.setVisibility(View.GONE);
            loginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(final Animator animation) {
                            loginFormView.setVisibility(View.GONE);
                        }
                    });
        } else {
            loginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(final Animator animation) {
                            loginFormView.setVisibility(View.VISIBLE);
                        }
                    });
            loginFormView.setVisibility(View.VISIBLE);
        }

        // Setup progress view.
        if (show) {
            progressView.setVisibility(View.VISIBLE);
            progressView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(final Animator animation) {
                            progressView.setVisibility(View.VISIBLE);
                        }
                    });
        } else {
            progressView.setVisibility(View.GONE);
            progressView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(final Animator animation) {
                            progressView.setVisibility(View.GONE);
                        }
                    });
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, LoginResult> {

        /**
         * Organization name for login.
         */
        private final String org;

        /**
         * Organization ID for login.
         */
        private final String orgId;

        /**
         * Username for login.
         */
        private final String userId;

        /**
         * Password for login.
         */
        private final String password;

        /**
         * Time util for measuring timings.
         */
        private TimeUtil util;

        /**
         * Create a user login task with the given organization,
         * username and password.
         *
         * @param userOrg   Organization
         * @param userOrgId Organization ID
         * @param username  Username
         * @param userPass  Password
         */
        UserLoginTask(final String userOrg,
                      final String userOrgId,
                      final String username,
                      final String userPass) {
            this.org = userOrg;
            this.orgId = userOrgId;
            this.userId = username;
            this.password = userPass;
        }

        @Override
        protected final LoginResult doInBackground(final Void... params) {

            //Create post parameters to be sent to server
            HashMap<String, String> nameValuePairs = new HashMap<>();
            nameValuePairs.put(AppConstant.LOGIN_ORG, orgId);
            nameValuePairs.put(AppConstant.LOGIN_USERNAME, userId);
            nameValuePairs.put(AppConstant.LOGIN_PASSWORD, password);

            Gson gson = new Gson();
            String jsonData = gson.toJson(nameValuePairs);

            //URL to request LDAP login
            String url = AppConstant.getBaseUrl() + AppConstant.LOGIN_URL;

            util = new TimeUtil();
            Response jsonResponse =
                    ServerInterface.getHttpResponse(url, jsonData, util);


            if (jsonResponse == null) {
                return LoginResult.NETWORK_ERROR;
            }

            if (jsonResponse.code() == AppConstant.HTTP_BAD_REQUEST) {
                jsonResponse.close();
                return LoginResult.WRONG_CREDENTIALS;
            }

            if (jsonResponse.code() != AppConstant.HTTP_OK) {
                jsonResponse.close();
                return LoginResult.NETWORK_ERROR;
            }

            Type type = new TypeToken<Map<String, String>>() {
            }
                    .getType();

            try {
                Map<String, String> map = gson.fromJson(
                        jsonResponse.body().string().trim(), type);

                if (map.containsKey("token")) {
                    AppData.setLoginToken(map.get("token"));
                    AppData.setLoginUsername(map.get("username"));
                    AppData.setLoginName(map.get("name"));
                    jsonResponse.close();
                    return LoginResult.SUCCESS;
                }

                jsonResponse.close();
                return LoginResult.WRONG_CREDENTIALS;

            } catch (IOException e) {
                e.printStackTrace();
                logError(logTag(), e);

                jsonResponse.close();
                return LoginResult.NETWORK_ERROR;
            }
        }

        @Override
        protected final void onPostExecute(final LoginResult success) {
            authTask = null;
            showProgress(false);

            if (success == LoginResult.NETWORK_ERROR) {

                Snackbar.make(findViewById(R.id.login_activity),
                        "Could not connect to server! \n "
                                + "Please check your internet connection",
                        Snackbar.LENGTH_LONG).show();
                return;
            }

            if (success == LoginResult.WRONG_CREDENTIALS) {
                Snackbar.make(
                        findViewById(R.id.login_activity),
                        "Please check your credentials and try again.",
                        Snackbar.LENGTH_LONG
                ).show();
                return;
            }

            // Save login for future
            SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(getApplication());
            sharedPrefs.edit().putString(AppConstant.LOGIN_USERNAME,
                    userId).apply();
            sharedPrefs.edit().putString(AppConstant.LOGIN_ORG,
                    org).apply();

            if (savePassword) {
                sharedPrefs.edit().putString(AppConstant.LOGIN_PASSWORD,
                        password).apply();
            } else {
                sharedPrefs.edit().putString(AppConstant.LOGIN_PASSWORD,
                        "").apply();
            }

            Intent intent = new Intent(getApplicationContext(),
                    QuizSelection.class);
            startActivity(intent);
        }

        @Override
        protected final void onCancelled() {
            authTask = null;
            showProgress(false);
        }
    }

    /**
     * Enum for Result from UserLoginTask.
     */
    enum LoginResult {

        /**
         * The task is a success and we got a token.
         */
        SUCCESS,

        /**
         * The task failed because of incorrect credentials.
         */
        WRONG_CREDENTIALS,

        /**
         * The task failed because of a network error.
         */
        NETWORK_ERROR
    }

    /**
     * Asynchronous call to retrieve organization list from the server.
     */
    public class RetrieveOrgListTask extends AsyncTask<Void, Void, Boolean> {

        /**
         * Time util for measuring timings.
         */
        private TimeUtil util;

        @Override
        protected final void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        @Override
        protected final Boolean doInBackground(final Void... voids) {
            String url = AppConstant.getBaseUrl() + AppConstant.ORG_LIST_URL;

            util = new TimeUtil();
            Response jsonResponse =
                    ServerInterface.getHttpResponse(url, null, util);

            if (jsonResponse == null
                    || jsonResponse.code() != AppConstant.HTTP_OK) {
                return false;
            }

            Gson gson = new Gson();

            Type type = new TypeToken<
                    Map<String, ArrayList<Map<String, String>>>>() {
            }
                    .getType();

            try {
                Map<String, ArrayList<Map<String, String>>> map = gson.fromJson(
                        jsonResponse.body().string().trim(), type);

                jsonResponse.close();

                if (!map.containsKey("results")) {
                    return false;
                }

                ArrayList<Map<String, String>> listOfOrgs = map.get("results");
                organizationList.clear();

                for (Map<String, String> org : listOfOrgs) {
                    organizationList.put(org.get("name"), org.get("id"));
                }

                return true;

            } catch (IOException e) {
                e.printStackTrace();
                logError(logTag(), e);

                jsonResponse.close();
                return false;
            }
        }

        @Override
        protected final void onPostExecute(final Boolean success) {
            orgListTask = null;
            super.onPostExecute(success);

            if (!success) {
                Snackbar.make(
                        findViewById(R.id.login_activity),
                        "Cannot load organizations. Network not available.",
                        Snackbar.LENGTH_LONG).show();
                showProgress(false);
                return;
            }

            orgViewAdapter.clear();
            orgViewAdapter.addAll(organizationList.keySet());
            orgViewAdapter.notifyDataSetChanged();
            orgViewSpinner.setText(savedOrg);

            showProgress(false);
        }

        @Override
        protected final void onCancelled() {
            super.onCancelled();
            orgListTask = null;
            showProgress(false);
        }
    }
}
