package com.iitb.cse.arkenstone.safe.quiz.services;

/**
 * Created by sonika on 05/10/16.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;

import java.util.Locale;

import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.QuizActivity;
import com.iitb.cse.arkenstone.safe.quiz.QuizLogging;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;
import com.iitb.cse.arkenstone.safe.utils.listeners.CallListener;

public class QuizMonitorService extends Service {

    public static QuizMonitorService instance = null;

    //Tag for debugging only
    private static final String TAG = "QuizMonitorService";

    //Notification
    static NotificationManager mNotificationManager;
    static Notification notification;

    private static boolean isRunning = false;
    private static boolean alertVisible = false;

    private static WindowManager windowManager;
    private static View overlayView;
    private static WindowManager.LayoutParams params;

    private static CountDownTimer timer;
    private static long defaultTimeout = 10, remainingTimeout; //Seconds

    private static Timer timerPhone;
    private static TimerTask timerTaskPhone;

    // Target we publish for clients to send messages to IncomingHandler.
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    private static long appFocusLostInstant = -1, totalFocusLostTime = 0;
    private static int appInterruptCount = 0; //Number of times the app went out of focus for any reasons.

    public QuizMonitorService() {
        if (instance == null)
            instance = this;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    /*Create the alert to be shown but don't show it yet*/
    void createAlert() {

        overlayView = LayoutInflater.from(this).inflate(R.layout.alert_overlay, null);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.START;
        params.x = 0;
        params.y = 100;

        Button alertButton = (Button) overlayView.findViewById(R.id.alert_button);
        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logDebug(logTag(), TAG, "On Click alert button");
                Intent myIntent = new Intent(getApplicationContext(), QuizActivity.class);
                // as QuizActivity launch_mode is defined as single_task,new activity will not be
                // created instead it resumes the activity which already exists in other task
                // for more information refer to
                // http://developer.android.com/guide/components/tasks-and-back-stack.html
                // #TaskLaunchModes
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
            }
        });

        overlayView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                logDebug(logTag(), TAG, "On touch overlay");
                Intent myIntent = new Intent(getApplicationContext(), QuizActivity.class);
                // as QuizActivity launch_mode is defined as single_task, new activity will not be
                // created instead it resumes the activity which already exists in other task
                // for more information refer
                // to "http://developer.android.com/guide/components/tasks-and-back-stack.html"
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
                return true;
            }
        });
    }

    void createTimer(final long sec) {
        timer = new CountDownTimer(sec * 1000, 1000) {
            TextView timerView, detailMsg;

            @Override
            public void onTick(long millisUntilFinished) {
                logDebug(logTag(), TAG, "OnTick");
                timerView = (TextView) overlayView.findViewById(R.id.alert_timer);
                detailMsg = (TextView) overlayView.findViewById(R.id.alert_details);

                if (remainingTimeout-- <= 0) {
                    this.onFinish();
                    this.cancel();
                }

                String counter = String.format(Locale.getDefault(),"00:%02d sec", remainingTimeout);
                timerView.setText(counter);

                detailMsg.setText(R.string.alert_detail_msg);
            }

            @Override
            public void onFinish() {
                logDebug(logTag(), TAG, "OnFinish");
                String text = "00:00 sec";
                timerView.setText(text);
                QuizLogging.newLog(new QuizLogging.QuizLog(
                        "Ignored " + defaultTimeout + " second dont leave quiz warning." +
                                "(Interrupt Count :" + appInterruptCount + ")",
                        QuizLogging.QuizLog.Severity.CRITICAL));
                text = "This event is reported to Instructor!";
                detailMsg.setText(text);

                remainingTimeout = 0; //Don't show timer again
            }
        };
    }

    /*User went out for shopping :P */
    void initAbsence() {
        appFocusLostInstant = SystemClock.elapsedRealtime(); //Note the time when focus is lost
    }

    /*Show alert on top of everything (except notification drawer)*/
    void showAlert() {
        if (alertVisible) {
            return;
        }

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        if (windowManager == null) return;
        windowManager.addView(overlayView, params);

        if (remainingTimeout > 0) {
            //Timer was not shown before
            logDebug(logTag(), "countdown timer stared");
            timer.start();
        }

        alertVisible = true;
    }

    /*Oops he came back after missing, lets report to police*/
    public void reportAbsence() {

        if (appFocusLostInstant < 0) return; //First time quiz gets focus

        long focusLostTime = (SystemClock.elapsedRealtime() - appFocusLostInstant) / 1000;
        appFocusLostInstant = -1; //Disable multiple log reports fom sudden events

        totalFocusLostTime += focusLostTime; //Add to total focus lost count


        //Report to server when user comes back
        if (focusLostTime <= 10)
            QuizLogging.newLog(new QuizLogging.QuizLog(
                    "App was not focused for " + focusLostTime +
                            " seconds! (Total time : " + totalFocusLostTime + "sec) " +
                            "(Interrupt Count :" + appInterruptCount + ")",
                    QuizLogging.QuizLog.Severity.WARNING));
        else
            QuizLogging.newLog(new QuizLogging.QuizLog(
                    "App was not focused for " + focusLostTime +
                            " seconds! (Total time : " + totalFocusLostTime + "sec) " +
                            "(Interrupt Count :" + appInterruptCount + ")",
                    QuizLogging.QuizLog.Severity.CRITICAL));
    }

    /*Send a notification if he is busy in notification drawer or if alert didn't work*/
    void showOutOfFocusNotification() {
        logDebug(logTag(), TAG, "showOutOfFocusNotification");
        String message = "You should not leave Quiz App while quiz is progressing! This event will be reported!";
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle("You Left Quiz App!") // title for notification
                .setContentText(message) // message for notification
                .setAutoCancel(true)// clear notification after click
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setOngoing(true); //Don't let user cancel it

        Intent intent = new Intent(this, QuizActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notification = mBuilder.build();
        mNotificationManager.notify(64, notification);
    }

    /*He came back, hide it*/
    public void removeAlert() {
        logDebug(logTag(), TAG, "removeAlert");
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        try {
            if (overlayView != null && windowManager != null && alertVisible) {
                windowManager.removeView(overlayView);
                timer.cancel();
                alertVisible = false;
            }
        } catch (Exception e) {
            logError(logTag(), e);
            e.printStackTrace();
        }
    }

    public void cancelNotification() {
        logDebug(logTag(), TAG, "cancel notification");
        if (notification != null) {
            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(64);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;

        createAlert();
        createTimer(defaultTimeout);

        logDebug(logTag(), TAG, "Service Started");

        startTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;

        logDebug(logTag(), TAG, "Service stopped!");

        stoptimertask();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logDebug(logTag(), TAG, "Received start id " + startId + ": " + intent);
        return START_STICKY; // run until explicitly stopped.
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    /*This receives message from Quiz UI*/
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            logDebug(logTag(), TAG, "Handling Message: " + msg.what);
            switch (msg.what) {

                case AppConstant.MSG_UI_IN_FOCUS:
                    cancelNotification();
                    removeAlert();
                    reportAbsence();
                    break;

                case AppConstant.MSG_UI_OUT_OF_FOCUS:
                    if (alertVisible) return;
                    initAbsence();
                    showOutOfFocusNotification();
                    showAlert();
                    appInterruptCount++;
                    break;

                case AppConstant.MSG_CONNECTED:
                    appInterruptCount = 0;
                    appFocusLostInstant = -1;
                    remainingTimeout = defaultTimeout;
                    break;

                case AppConstant.MSG_DISCONNECTED:
                    cancelNotification();
                    removeAlert();
                    appFocusLostInstant = -1;
                    appInterruptCount = 0;
                    totalFocusLostTime = 0;
                    remainingTimeout = defaultTimeout;
                    break;

                default:
                    super.handleMessage(msg);
            }
            Log.d(TAG, "Message Handled");
        }
    }

    public void startTimer() {
//        if(!QuizMonitorService.isRunning()) {
//            logDebug(logTag(), "From start timer quizmonitorservice stop");
//            stoptimertask();
//            return;
//        }

        if(timerPhone != null){
            logDebug(logTag(), "From timer null stop");
            stoptimertask();
        }
        logDebug(logTag(), "Timer started!");
        //set a new Timer
        timerPhone = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 5000ms
        timerPhone.schedule(timerTaskPhone, 5000, 5000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timerPhone != null) {
            timerPhone.cancel();
            timerPhone = null;
            logDebug(logTag(), "Timer stopped!");
        }
    }

    public void initializeTimerTask() {

        timerTaskPhone = new TimerTask() {
            public void run() {
                if(CallListener.getPhoneState() == 1) {
                    QuizLogging.newLog(new QuizLogging.QuizLog("Phone is ringing", QuizLogging.QuizLog.Severity.CRITICAL));
                } else if(CallListener.getPhoneState() == 2) {
                    QuizLogging.newLog(new QuizLogging.QuizLog("Phone call in progress (Call accepted)", QuizLogging.QuizLog.Severity.CRITICAL));
                }
            }
        };
    };
}
