package com.iitb.cse.arkenstone.safe;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.quiz.QuizActivity;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.widget.EndlessRecyclerViewScrollListener;

import okhttp3.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PreviousSubmissionActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProgressDialog dialog;
    LinearLayoutManager layoutManager;
    PreviousSubmissionAdapter adapter;

    boolean loadComplete = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (ContextHelper.isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_submission);

        recyclerView = (RecyclerView) findViewById(R.id.previous_submission_recycler_view);
        recyclerView.setHasFixedSize(true);

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Fetching data from server");
        dialog.setCancelable(false);
        dialog.show();

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new PreviousSubmissionAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                fetchPreviousSubmissions(page, totalItemsCount);
            }
        });
        fetchPreviousSubmissions(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialog.dismiss();
    }

    void fetchPreviousSubmissions(final int page, final int totalItemsCount) {
        if (loadComplete) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                String url = AppConstant.getBaseUrl() + AppConstant.SHORT_SUBMISSION_HISTORY_URL;
                url += "?page=" + page;

                Response httpResponse = ServerInterface.getHttpResponse(url, null, null);
                String data;
                try {
                    if (httpResponse == null) {
                        logError(logTag(), "HTTP Response is null. This is unexpected");
                        return null;
                    } else {
                        data = httpResponse.body().string();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }

                Gson gson = new Gson();
                Type type = new TypeToken<Map<String, Object>>() {
                }
                        .getType();
                Map<String, Object> map = gson.fromJson(data, type);

                Type type2 = new TypeToken<ArrayList<PreviousSubmissionData>>() {

                }
                        .getType();
                JsonArray results = gson.toJsonTree(map.get("results")).getAsJsonArray();


                ArrayList<PreviousSubmissionData> deserializedData
                        = gson.fromJson(results, type2);

                if (deserializedData.isEmpty()) {
                    if (page == 0 && totalItemsCount == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PreviousSubmissionActivity.this,
                                        "You have no Previous Submissions to display!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                        finish();
                    }
                    loadComplete = true;
                    return null;
                } else {
                    Collections.reverse(deserializedData);
                }

                adapter.items.addAll(deserializedData);
                adapter.notifyItemRangeInserted(totalItemsCount, deserializedData.size());

                if (!map.containsKey("next") || map.get("next") == null) {
                    loadComplete = true;
                    return null;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void avoid) {
                if (dialog.isShowing()) {
                    dialog.hide();
                }
            }
        }.execute();
    }
}


/**
 * Custom Adapter for previous submissions.
 */
@SuppressWarnings("CheckStyle")
class PreviousSubmissionAdapter extends RecyclerView.Adapter<PreviousSubmissionAdapter.ViewHolder> {

    List<PreviousSubmissionData> items = new ArrayList<>();

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView uuid;
        public TextView quiz;
        public TextView date;
        public TextView marks;

        public ViewHolder(View itemView) {
            super(itemView);

            uuid = (TextView) itemView.findViewById(R.id.quiz_id);
            quiz = (TextView) itemView.findViewById(R.id.mini_summary);
            date = (TextView) itemView.findViewById(R.id.quiz_date);
            marks = (TextView) itemView.findViewById(R.id.quiz_marks);
        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_previous_submission, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        PreviousSubmissionData item = items.get(position);

        holder.uuid.setText(item.uuid);
        holder.quiz.setText(item.quiz);

        String marks;
        if (item.marksObtained == null) {
            marks = "Marks: " + "NA (" + item.totalMarks + ")";
        } else {
            marks = "Marks: " + item.marksObtained + " (" + item.totalMarks + ")";
        }
        holder.marks.setText(marks);

        Date date = ContextHelper.formatDate(item.startedAt);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        String dateStr = cal.get(Calendar.DATE) + "-"
                + cal.get(Calendar.MONTH) + "-"
                + cal.get(Calendar.YEAR);
        holder.date.setText(dateStr);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, PreviousQuizActivity.class);
                intent.putExtra("uuid", holder.uuid.getText().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

/**
 * Structure for the previous submissions.
 */
@SuppressWarnings("CheckStyle")
class PreviousSubmissionData {
    public String uuid;

    @SerializedName("started_at")
    public String startedAt;

    @SerializedName("total_marks")
    public Float totalMarks;

    public String quiz;

    @SerializedName("marks_obtained")
    public Float marksObtained;
}
