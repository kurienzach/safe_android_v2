package com.iitb.cse.arkenstone.safe.utils.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * Custom spinner class which is used in place of the original Spinner
 * class. Uses text view to display and PopupWindow for selection objects.
 */
public class SafeSpinner extends EditText {

    /**
     * The popup window which holds the listview.
     */
    private PopupWindow popupWindow;

    /**
     * The listview which shows up as the popup.
     */
    private ListView optionsListView;

    /**
     * On create, we run loadOptions which loads all the custom parts of the
     * application.
     *
     * @param context The context of creation.
     */
    private void loadOptions(final Context context) {
        setFocusable(false);
        setLongClickable(false);
        setCursorVisible(false);

        // Initialize a pop up window type
        popupWindow = new PopupWindow(context);

        // the drop down list is a list view
        optionsListView = new ListView(context);
        optionsListView.setBackgroundColor(Color.WHITE);

        // set other visual settings
        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(optionsListView);

        this.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                popupWindow.showAsDropDown(view, 0, 0);
            }
        });

        optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView,
                                    final View view, final int arg1, final long arg2) {
                Animation fadeInAnimation = AnimationUtils.loadAnimation(view.getContext(),
                        android.R.anim.fade_in);
                fadeInAnimation.setDuration(10);
                view.startAnimation(fadeInAnimation);

                popupWindow.dismiss();

                String selectedText = ((TextView) view).getText().toString();
                SafeSpinner.this.setText(selectedText);
            }
        });

    }

    public SafeSpinner(final Context context) {
        super(context);
        loadOptions(context);
    }

    public SafeSpinner(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        loadOptions(context);
    }

    public SafeSpinner(final Context context,
                       final AttributeSet attrs,
                       final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        loadOptions(context);
    }

    public final void setAdapter(final ListAdapter adapter) {
        optionsListView.setAdapter(adapter);
    }

    /**
     * An adapter used for the display of list.
     *
     * @param context Context of application.
     * @return Adapter that would be used for setAdapter purposes.
     */
    public static ArrayAdapter<String> createSpinnerAdapter(final Context context) {
        return new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1
        ) {

            @Override
            public View getView(final int position,
                                final View convertView,
                                final ViewGroup parent) {

                String item = getItem(position);

                TextView listItem = new TextView(getContext());
                listItem.setWidth(parent.getWidth());
                listItem.setBackgroundColor(Color.WHITE);
                listItem.setTextColor(Color.BLACK);
                listItem.setSingleLine(true);
                listItem.setPadding(17, 17, 17, 17);

                listItem.setText(item);
                return listItem;
            }
        };
    }
}
