package com.iitb.cse.arkenstone.safe.quiz.questions.data.types;

import com.google.gson.annotations.SerializedName;

import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for a question with multiple options with
 * multiple correct answers.
 */
public class McqMultipleAnswer extends Question {

    /**
     * The options for the question.
     */
    public ArrayList<String> options;

    /**
     * Shuffled options.
     */
    public transient ArrayList<String> shuffledOptions;

    /**
     * Submitted response for the question
     * in case of PreviousQuiz.
     */
    @SerializedName("submitted_response")
    public ArrayList<Integer> submittedResponse;

    /**
     * List of answers in a question.
     * in case of PreviousQuiz
     */
    @SerializedName("answers")
    public ArrayList<ArrayList<Integer>> answers = null;

    /**
     * Should randomize the options or not.
     */
    @SerializedName("randomize_options")
    public Boolean randomizeOptions;

    public McqMultipleAnswer() {
        this.type = AppConstant.TYPE_MCQ_MULTIPLE_ANSWER;
    }

    @Override
    public void post_save() {
        shuffledOptions = new ArrayList<>(options);
        if (randomizeOptions) {
            Collections.shuffle(shuffledOptions);
        }
    }
}
