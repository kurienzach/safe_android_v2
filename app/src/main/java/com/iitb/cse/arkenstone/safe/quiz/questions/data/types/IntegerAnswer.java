package com.iitb.cse.arkenstone.safe.quiz.questions.data.types;

import com.google.gson.annotations.SerializedName;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for question with integer as answer.
 */
public class IntegerAnswer extends Question {

    /**
     * Submitted response for the question
     * in case of PreviousQuiz.
     */
    @SerializedName("submitted_response")
    public ArrayList<Integer> submittedResponse;

    /**
     * List of answers in a question.
     * in case of PreviousQuiz
     */
    @SerializedName("answers")
    public ArrayList<Integer> answers = null;

    public IntegerAnswer() {
        this.type = AppConstant.TYPE_INTEGER_ANSWER;
    }

    @Override
    public void post_save() {
    }
}
