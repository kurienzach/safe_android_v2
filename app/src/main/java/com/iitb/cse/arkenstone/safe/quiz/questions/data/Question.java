package com.iitb.cse.arkenstone.safe.quiz.questions.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.iitb.cse.arkenstone.safe.quiz.QuizSubmissions;

import java.util.ArrayList;

/**
 * The base question class which is the root class for
 * all the other types of questions.
 */
public abstract class Question {

    /**
     * The reason text length for this question.
     */
    @SerializedName("reason_text_length")
    public Integer reasonTextLength;

    /**
     * Description of the question. This is the actual
     * question text.
     */
    public String description;

    /**
     * ID of the question. Used to submit about the
     * answer by the student to the instructor. This is
     * exposed because the question id should be sent as
     * a part of the response.
     */
    @Expose
    @SerializedName("question")
    public Integer id;

    /**
     * This is the response object. This is to be sent
     * but not received. So, we add an expose which
     * explicitly sends the data and making it transient
     * so that it is excluded while deserializing. This
     * works because we use two different Gson Builders.
     */
    @Expose
    private transient String response = "";

    /**
     * Max marks for the question.
     */
    @SerializedName("max_marks")
    public Float maxMarks;

    /**
     * The type of the question. This is instantiated
     * in the child class.
     */
    public String type;

    /**
     * The generated question number.
     */
    public transient String questionNumber = "";

    /**
     * The module to which the question belongs.
     */
    public transient Module module;

    /**
     * Reason text for the question response
     * in case of PreviousQuiz
     */
    @SerializedName("reason_text")
    public String reasonText = "";



    /**
     * Marks obtained in a question.
     * in case of PreviousQuiz
     */

    @SerializedName("marks_obtained")
    public Float marksObtained = null;

    public abstract void post_save();

    public String getResponse() {
        return response;
    }

    /**
     * Set the response for the question. Note that this
     * also calls the function which attempts the periodic
     * submission.
     *
     * @param response The response for this question.
     */
    public void setResponse(String response) {
        this.response = response;
        QuizSubmissions.instance.tryPeriodicSubmission();
    }

}
