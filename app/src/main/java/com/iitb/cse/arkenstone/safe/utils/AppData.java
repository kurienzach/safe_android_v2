package com.iitb.cse.arkenstone.safe.utils;

import com.iitb.cse.arkenstone.safe.utils.widget.WebViewCache;

import java.util.concurrent.Semaphore;

/**
 * Singleton to handle dynamic data used across application.
 */
public final class AppData {

    /**
     * The instance of the singleton class.
     */
    private static final AppData instance = new AppData();

    /********* USER LOGIN DETAILS. ***************/

    /**
     * Token received after successful login.
     */
    private static String loginToken;

    /**
     * WebViewCache used in QuizActivity and PreviousQuizActivity instead of saving all web views.
     */
    private static WebViewCache webViewCache;

    /**
     * Login username.
     */
    private static String loginUsername;

    /**
     * Login Name for the user.
     */
    private static String loginName;

    /********* USER LOGIN DETAILS END. ***********/

    /************** QUIZ TRACKING. ***************/

    /**
     * Track whether quiz is started or not.
     */
    private static Boolean quizStarted = false;

    /************ QUIZ TRACKING END. *************/

    /**
     * Constructor to defeat instantiation.
     */
    private AppData() {
        // Body not required
    }


    /**
     * Get the login token after successful login.
     *
     * @return Login token
     */
    public static String getLoginToken() {
        return loginToken;
    }

    /**
     * Get the webView cache that stores the web views.
     *
     * @return WebViewCache
     */
    public static WebViewCache getWebViewCache() {
        return webViewCache;
    }

    /**
     * Set the loginToken after successful login.
     * This would be persisted throughout the session.
     *
     * @param token New login token
     */
    public static void setLoginToken(final String token) {
        AppData.loginToken = token;
    }

    /**
     * Set the webView Cache that stores the web views.
     *
     * @param webViewCache New WebViewCache
     */
    public static void setWebViewCache(final WebViewCache webViewCache) {
        AppData.webViewCache = webViewCache;
    }


    /**
     * Is the quiz started?
     *
     * @return True if yes, false otherwise.
     */
    public static Boolean isQuizStarted() {
        return quizStarted;
    }

    /**
     * Set the quiz start mode.
     *
     * @param isStarted True if quiz started, false otherwise.
     */
    public static void setQuizStarted(final Boolean isStarted) {
        AppData.quizStarted = isStarted;
    }

    /**
     * Get the login username for the user.
     * This is generally the username he entered in the screen.
     *
     * @return Username
     */
    public static String getLoginUsername() {
        return loginUsername;
    }

    /**
     * Set the login username received from the server.
     *
     * @param loginUsername The username to which it must be set to.
     */
    public static void setLoginUsername(String loginUsername) {
        AppData.loginUsername = loginUsername;
    }

    /**
     * Get the name of the user.
     *
     * @return Name of the user.
     */
    public static String getLoginName() {
        return loginName;
    }

    /**
     * Set the name of the user.
     *
     * @param loginName Name to which this must be set.
     */
    public static void setLoginName(String loginName) {
        AppData.loginName = loginName;
    }
}
