package com.iitb.cse.arkenstone.safe.quiz.questions.data.types;

import com.google.gson.annotations.SerializedName;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for question with decimal value as answer.
 */
public class FloatAnswer extends Question {

    /**
     * Submitted response for the question
     * in case of PreviousQuiz.
     */
    @SerializedName("submitted_response")
    public Float submittedResponse;

    /**
     * List of answers in a question.
     * in case of PreviousQuiz
     */
    @SerializedName("answers")
    public ArrayList<Float> answers = null;

    public FloatAnswer() {
        this.type = AppConstant.TYPE_FLOAT_ANSWER;
    }

    @Override
    public void post_save() {
    }
}
