package com.iitb.cse.arkenstone.safe;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.iitb.cse.arkenstone.safe.network.NetworkAsyncOperation;
import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.quiz.QuizActivity;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.AppData;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.NetworkUtils;
import com.iitb.cse.arkenstone.safe.utils.TimeUtil;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Activity for selecting a quiz or accessing previous submissions.
 */
public class QuizSelection extends Activity implements NetworkAsyncOperation {

    /**
     * Time util for Network Async Operation.
     */
    private TimeUtil timeUtil;

    /**
     * The quizId text field.
     */
    private static EditText quizId;

    /**
     * The form to enter quizId.
     */
    private View quizIdForm;

    /**
     * Progress bar for the quiz selection activity.
     */
    private ProgressBar quizSelectionProgress;

    /**
     * Change the behaviour of back button as deemed necessary.
     */
    private volatile Boolean canClickBackButton;

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {

        if (ContextHelper.isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_quiz_selection);
        ContextHelper.setupHideKeyboardOnOutsideTouch(findViewById(R.id.quiz_selection), this);

        // Set username display
        EditText userName = (EditText) findViewById(R.id.user_id);
        String text = AppData.getLoginName() + " (" + AppData.getLoginUsername() + ")";
        userName.setText(text);

        Button submitButton = (Button) findViewById(R.id.connect_button);
        submitButton.getBackground().setColorFilter(
                Color.parseColor("#4169E1"), PorterDuff.Mode.MULTIPLY
        );
        submitButton.setTextColor(Color.WHITE);

        quizId = (EditText) findViewById(R.id.quiz_id);
        quizId.requestFocus();
        quizId.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        quizIdForm = findViewById(R.id.quiz_id_form);
        quizSelectionProgress = (ProgressBar) findViewById(R.id.quiz_selection_progress);
        quizSelectionProgress.setIndeterminate(true);

        canClickBackButton = true;

        if (ContextHelper.logFloatingApps()) {
            Snackbar.make(
                    findViewById(R.id.quiz_selection),
                    getString(R.string.floating_apps_detected),
                    Snackbar.LENGTH_LONG
            ).show();
        }

        timeUtil = new TimeUtil();
    }

    @Override
    public final TimeUtil getTimeUtil() {
        if (timeUtil == null) {
            timeUtil = new TimeUtil();
        }
        return timeUtil;
    }

    @Override
    public void onBackPressed() {
        if (canClickBackButton) {
            super.onBackPressed();
        }
    }

    /**
     * Show the progress dialog.
     *
     * @param show Show the dialog if true.
     */
    private void showProgress(Boolean show) {

        if (show) {
            canClickBackButton = false;
            quizIdForm.setVisibility(View.GONE);
            quizSelectionProgress.setVisibility(View.VISIBLE);
        } else {
            canClickBackButton = true;
            quizIdForm.setVisibility(View.VISIBLE);
            quizSelectionProgress.setVisibility(View.GONE);
        }
    }

    /**
     * Submit quiz id button clicked
     *
     * @param view The view of the click.
     */
    public void submitQuizId(View view) {
        quizId.setError(null);

        //Check network connectivity
        if (!NetworkUtils.isNetworkAvailable()) {

            Snackbar.make(
                    findViewById(R.id.quiz_selection),
                    "No network connection!",
                    Snackbar.LENGTH_LONG
            ).show();
            return;
        }

        //If bluetooth and mobile data are on, do not continue.
        if (!ContextHelper.isEmulator() && !NetworkUtils.externalConnectionOff(this, false)) {
            //A Toast will be shown by the method
            return;
        }

        // Validate inputs
        String quizId = QuizSelection.quizId.getText().toString();

        if (quizId.isEmpty()) {
            QuizSelection.quizId.setError("Quiz ID cannot be empty!");
            QuizSelection.quizId.findFocus();
            return;
        }

        showProgress(true);

        String url = AppConstant.getBaseUrl() + AppConstant.QUIZ_AUTH_URL;
        url = String.format(url, quizId);
        ContextHelper.setupHideKeyboardOnOutsideTouch(view, QuizSelection.this);
        new ServerInterface.NetworkOperation().execute(url, this, null, "AUTH_GET");
    }

    @Override
    public final void doneNetworkOperation(final String tag,
                                           final String response,
                                           final Integer responseCode) {
        if ("AUTH_GET".equals(tag)) {
            showProgress(false);

            if (responseCode == null) {
                Snackbar.make(
                        findViewById(R.id.quiz_selection),
                        "Network Error. Try again later",
                        Snackbar.LENGTH_LONG).show();
                return;
            }

            if (responseCode == 404) {
                Snackbar.make(
                        findViewById(R.id.quiz_selection),
                        "Unknown quiz. Check the input",
                        Snackbar.LENGTH_LONG).show();
                return;
            }

            if (responseCode == 401 || responseCode == 403) {
                Snackbar.make(
                        findViewById(R.id.quiz_selection),
                        "You are not authorized",
                        Snackbar.LENGTH_LONG).show();
            }

            if (responseCode != 200) {
                Snackbar.make(
                        findViewById(R.id.quiz_selection),
                        "Unknown network error!",
                        Snackbar.LENGTH_LONG).show();
                return;
            }

            // Extract the data received
            Gson gson = new Gson();
            Type type = new TypeToken<Map<String, Object>>() {
            }
                    .getType();
            Map<String, Object> map = gson.fromJson(response, type);

            if (!((Boolean) map.get("success"))) {
                Snackbar.make(
                        findViewById(R.id.quiz_selection),
                        "Request failed at server! Contact instructor",
                        Snackbar.LENGTH_LONG
                ).show();
                return;
            }
            logDebug(logTag(), response);

            if (map.get("keyboard") == null) {
                showProgress(true);

                String url = AppConstant.getBaseUrl() + AppConstant.QUIZ_AUTH_URL;
                url = String.format(url, quizId.getText().toString());
                new ServerInterface.NetworkOperation().execute(url, this, "{}", "AUTH_POST");
                return;
            }
            // TODO Do something for visual password.
        } else if ("AUTH_POST".equals(tag)) {

            // Auth Successful
            if (responseCode == AppConstant.HTTP_OK) {
                // Extract the data received
                Gson gson = new Gson();
                Type type = new TypeToken<Map<String, Object>>() {
                }
                        .getType();
                Map<String, Object> map = gson.fromJson(response, type);

                if (!((Boolean) map.get("success"))) {
                    Snackbar.make(
                            findViewById(R.id.quiz_selection),
                            "Request failed at server! Contact instructor",
                            Snackbar.LENGTH_LONG
                    ).show();
                    showProgress(false);
                    return;
                }

                logDebug(logTag(), response);
                Intent intent = new Intent(this, QuizActivity.class);
                intent.putExtra("quizKey", quizId.getText().toString());
                startActivity(intent);
                finish();
                return;
            }

            showProgress(false);
        }
    }

    /**
     * Function to call on clicking previous submissions.
     *
     * @param view The button view which called this function
     */
    public void prevSubmissions(View view) {
        startActivity(new Intent(this, PreviousSubmissionActivity.class));
    }
}
