package com.iitb.cse.arkenstone.safe.quiz;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logError;
import static com.iitb.cse.arkenstone.safe.utils.Logging.logTag;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.network.ServerInterface;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Module;
import com.iitb.cse.arkenstone.safe.quiz.questions.fragments.QuestionFragment;
import com.iitb.cse.arkenstone.safe.quiz.services.QuizCountdownService;
import com.iitb.cse.arkenstone.safe.utils.AppConstant;
import com.iitb.cse.arkenstone.safe.utils.ContextHelper;
import com.iitb.cse.arkenstone.safe.utils.widget.WebViewCache;

import okhttp3.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Semaphore;


/**
 * The singleton class with loads the quiz from
 * server and populates * the quiz data with
 * questions and options.
 */
public class QuizLoader {

    /**
     * Singleton instance.
     */
    private static QuizLoader instance;

    /**
     * The QuizActivity instance to which this class should be
     * associated with.
     */
    private static QuizActivity quizActivity;

    /**
     * Key used to enter the current quiz.
     */
    private static String quizKey;

    /**
     * An object used for maintaining whether quiz
     * interface loading has started or not.
     */
    private static Boolean quizInterfaceStarted = false;

//    /**
//     * Semaphore for each web view. This is useful to know
//     * when all the web views are loaded and ready to display
//     */
//    public final Semaphore webViewCacheLock = new Semaphore(WebViewCache.WebViewCacheSize + 1);
//
//    /**
//     * Semaphore for the creation of webview. Make sure that the
//     * webview creation is done before we start quiz interface.
//     */
//    public final Semaphore webViewCacheCreate = new Semaphore(1);

    /**
     * Page change listener for the quiz adapter.
     */
    ViewPager.SimpleOnPageChangeListener onPageChangeListener;

    /**
     * Pager for the Quiz.
     */
    ViewPager quizPager;

    /**
     * Create the singleton instance with the activity.
     *
     * @param activity The instance of QuizActivity which
     *                 created this class.
     * @param key      Key for the quiz. Used for fetching the
     *                 questions.
     */
    public static void createInstance(QuizActivity activity, String key) {
        instance = new QuizLoader();
        quizActivity = activity;
        quizKey = key;
        quizInterfaceStarted = false;
    }

    /**
     * Return the instance of the Loading Screen.
     *
     * @return LoadingScreen's singleton instance
     */
    public static QuizLoader getInstance() {
        return instance;
    }

    /**
     * Enum to store the result of Quiz Fetch Job.
     */
    public enum FetchResult {

        /**
         * Generic failure. This can indicate
         * any failure. Look at logs for details.
         */
        FAILURE,

        /**
         * Fetch is successful and quiz has
         * already started.
         */
        QUIZ_STARTED,

        /**
         * We are outside straddle duration
         * and need to wait until we can download
         * the questions from the server.
         */
        OUTSIDE_STRADDLE,

        /**
         * We are inside straddle duration. This
         * means we also got hold of the questions.
         */
        INSIDE_STRADDLE,
    }

    /**
     * Fetch the quiz from server and process it.
     */
    public void fetchQuiz() {
        new AsyncTask<Void, Void, FetchResult>() {

            Map<String, Object> jsonResponse;

            @Override
            protected FetchResult doInBackground(Void... voids) {

                LoadingScreen.getInstance().addDebugInfo("Fetching quiz");

                String url = AppConstant.getBaseUrl() + AppConstant.QUIZ_FETCH_URL;
                url = String.format(url, quizKey);
                Response response = ServerInterface.getHttpResponse(url, null, null);

                if (response == null
                        || !response.isSuccessful()
                        || response.code() != AppConstant.HTTP_OK) {
                    if (response == null) {
                        LoadingScreen.getInstance().addDebugInfo("Response is null");
                    } else {
                        LoadingScreen.getInstance().addDebugInfo("Response: " + response.code());
                    }
                    return FetchResult.FAILURE;
                }

                Gson gson = new Gson();
                Type type = new TypeToken<Map<String, Object>>() {
                }
                        .getType();

                try {
                    jsonResponse = gson.fromJson(response.body().string(), type);
                } catch (IOException e) {
                    LoadingScreen.getInstance().addDebugInfo("Map deserialization failure");
                    logError(logTag(), "Map deserialization failure");
                    logError(logTag(), e);
                    e.printStackTrace();
                    return FetchResult.FAILURE;
                }

                LoadingScreen.getInstance().addDebugInfo("Retrieved quiz information");

                Date quizStartTime;
                Date currentTime;
                Date endTime;

                quizStartTime = ContextHelper
                        .formatDate((String) jsonResponse.get("start_time"));

                currentTime = ContextHelper
                        .formatDate((String) jsonResponse.get("current_time"));

                endTime = ContextHelper
                        .formatDate((String) jsonResponse.get("end_time"));

                if (currentTime == null || quizStartTime == null) {
                    logError(logTag(), "Current time or quiz start time is null");
                    return FetchResult.FAILURE;
                }

                if (endTime == null) {
                    Intent intent = new Intent(quizActivity, QuizCountdownService.class);
                    logDebug(logTag(), "Starting timer without time (infinite quiz)");
                    quizActivity.startService(intent);
                } else {
                    long timeRemainingMillis = endTime.getTime() - currentTime.getTime();
                    Intent intent = new Intent(quizActivity, QuizCountdownService.class);
                    intent.putExtra("timeLeft", timeRemainingMillis);

                    logDebug(logTag(), "Starting timer with "
                            + timeRemainingMillis / 1000 + " seconds");
                    quizActivity.startService(intent);
                }

                Integer straddleDuration;
                straddleDuration = ((Double) jsonResponse.get("straddle_duration")).intValue();
                QuizData.setStraddleDuration(straddleDuration);

                if (currentTime.compareTo(quizStartTime) >= 0) {
//                    LoadingScreen.getInstance().setCountDownDisplay("Loading Quiz");
//                    LoadingScreen.getInstance().addDebugInfo("Quiz already started.");
//                    return FetchResult.QUIZ_STARTED;
                    LoadingScreen.getInstance().createCountdownTimer(
                            5000
                    );
                } else {

                    // Start the countdown timer on the screen for the user
                    LoadingScreen.getInstance().createCountdownTimer(
                            quizStartTime.getTime() - currentTime.getTime()
                    );
                }

//                if (currentTime.getTime() + straddleDuration * 1000 < quizStartTime.getTime()) {
//                    jobTimerForFetch(
//                            quizStartTime.getTime() - currentTime.getTime(),
//                            straddleDuration
//                    );
//                    LoadingScreen.getInstance()
//                            .addDebugInfo("Outside straddle duration. Waiting");
//                    return FetchResult.OUTSIDE_STRADDLE;
//                }

//                LoadingScreen.getInstance()
//                        .addDebugInfo("Inside straddle duration. Load questions and wait");
//                return FetchResult.INSIDE_STRADDLE;
                return FetchResult.INSIDE_STRADDLE;
            }

            @Override
            protected void onPostExecute(final FetchResult fetchResult) {
                if (fetchResult == FetchResult.FAILURE) {
                    Snackbar.make(
                            quizActivity.findViewById(R.id.quiz_activity),
                            "Quiz fetch failed. Contact instructor.",
                            Snackbar.LENGTH_LONG
                    );
                    return;
                }

                if (fetchResult == FetchResult.INSIDE_STRADDLE
                        || fetchResult == FetchResult.QUIZ_STARTED) {

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            QuizData.parseQuestionsJson(jsonResponse);

                            // If quiz is started, we ask to schedule to
                            // start the interface. In other cases, the
                            // onscreen timer would invoke this function
//                            if (fetchResult == FetchResult.QUIZ_STARTED) {
//                                scheduleStartQuizInterface();
//                            }
                            return null;
                        }
                    }.executeOnExecutor(THREAD_POOL_EXECUTOR);
                }

                // We are outside straddle duration. This means we don't need to
                // do anything. We just wait.
            }
        }.execute();
    }

    /**
     * Start the countdown timer with a target time in the future.
     *
     * @param startTime        The time at which quiz should be started.
     * @param straddleDuration Straddle duration for the quiz (seconds).
     */
    public void jobTimerForFetch(long startTime, Integer straddleDuration) {

        long beforeStraddle = startTime - straddleDuration * 1000;
        long randomPositionInStraddle = (long) (Math.random() * straddleDuration * 1000);
        final long startMillis = beforeStraddle + randomPositionInStraddle;

        LoadingScreen.getInstance().addDebugInfo("Added job to fetch the questions in future");
        LoadingScreen.getInstance().addDebugInfo(
                String.format(Locale.ENGLISH, "Remaining time to fetch: %02d:%02d seconds",
                        startMillis / 1000 / 60, (startMillis / 1000) % 60));

        // Handler must be called from the UI thread. Therefore we are adding
        // this over that.
        // http://stackoverflow.com/questions/3072173/how-to-call-a-method-after-a-delay-in-android
        quizActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchQuiz();
                    }
                }, startMillis);
            }
        });
    }

    /**
     * Try to start the quiz interface and notify if there are any
     * problems that are blocking. This is run on a separate thread which
     * blocks until all the WebViews are ready.
     */
    public void scheduleStartQuizInterface() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                LoadingScreen.getInstance().addDebugInfo("Attempting to start quiz interface");

                //synchronized (this) {
                    if (quizInterfaceStarted) {
                        return;
                    } else {
                        quizInterfaceStarted = true;
                    }
                //}

                try {
                    LoadingScreen.getInstance().addDebugInfo("Waiting on WebView cache");
                    WebViewCache.webViewCacheCreate.acquire();
                    WebViewCache.webViewCacheLock.acquire(WebViewCache.WebViewCacheSize);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    LoadingScreen.getInstance().addDebugInfo("WebView Cache lock fail");
                    return;
                }

                LoadingScreen.getInstance().addDebugInfo("WebView Cache load complete");

                TabLayout tabLayout = (TabLayout) quizActivity.findViewById(R.id.quiz_tab_layout);
                ArrayList<Module> questionModules = QuizData.getQuestionModules();
                LoadingScreen.getInstance().addDebugInfo("Setting up modules and tabs");
                for (int i = 0; i < questionModules.size(); i++) {
                    questionModules.get(i).setModuleNumber(i + 1, tabLayout);
                }
                setupPager(tabLayout);
                WebViewCache.webViewCacheCreate.release();
                WebViewCache.webViewCacheLock.release(WebViewCache.WebViewCacheSize);
                logDebug(logTag(), "Quiz interface ready!");
                LoadingScreen.getInstance().hideLoadingScreen();
                quizActivity.setQuizStateToStarted();
            }
        }).start();
    }

    private void setupPager(final TabLayout tabLayout) {
        LoadingScreen.getInstance().addDebugInfo("Creating the quiz pager");

        quizPager = (ViewPager) quizActivity.findViewById(R.id.quiz_pager);
        final QuizPagerAdapter pagerAdapter = new QuizPagerAdapter(
                quizActivity.getSupportFragmentManager(),
                QuizData.getQuestionModules());

        QuizData.setQuizPagerAdapter(pagerAdapter);
        quizActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                quizPager.setAdapter(pagerAdapter);
                quizPager.setOffscreenPageLimit(4);
                tabLayout.setupWithViewPager(quizPager);

                onPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        super.onPageSelected(position);
                        QuestionFragment item = (QuestionFragment)
                                QuizData.getQuizPagerAdapter().getItem(position);
                        QuizFab.instance.hideFab();
                        item.setupFab();
                        if (QuizData.isQuizFrozen()) {
                            item.freezeQuestion();
                        }
                        QuizFab.instance.showFab();
                    }
                };
                quizPager.addOnPageChangeListener(onPageChangeListener);

                // Run the listener for the first page.
                quizPager.post(new Runnable() {
                    @Override
                    public void run() {
                        onPageChangeListener.onPageSelected(0);
                    }
                });
            }
        });
    }

    public void changePage(final int i) {
        quizActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                quizPager.setCurrentItem(i);
            }
        });
    }
}
