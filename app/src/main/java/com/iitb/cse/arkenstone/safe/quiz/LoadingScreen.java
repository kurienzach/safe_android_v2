package com.iitb.cse.arkenstone.safe.quiz;

import static com.iitb.cse.arkenstone.safe.utils.Logging.logDebug;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.iitb.cse.arkenstone.safe.R;

import java.util.Locale;

/**
 * Variables and methods required for the proper functioning
 * of the loading screen. Also contains methods for displaying
 * progress and debug information in the loading screen.
 * As there is only one activity, this is a singleton linked to
 * that particular QuizActivity.
 */
public class LoadingScreen {

    /**
     * Singleton instance.
     */
    private static LoadingScreen instance;

    /**
     * The QuizActivity instance to which this class should be
     * associated with.
     */
    private static QuizActivity quizActivity;

    /**
     * The indeterminate progress bar to display that work is
     * happening in the QuizActivity.
     */
    private ProgressBar progressBar;

    /**
     * The TextView to print the debugging data.
     */
    private TextView debugTextView;

    /**
     * The ScrollView which holds the debugging text box.
     */
    private ScrollView debugTextScroll;

    /**
     * User displayed countdown on the loading screen.
     */
    private TextView countDown;

    /**
     * Countdown timer to show on the loading screen.
     */
    private CountDownTimer countDownTimer;

    /**
     * String to the complete debug text.
     */
    private volatile String debugText = "";

    /**
     * Create the singleton instance with the activity.
     *
     * @param activity The instance of QuizActivity which
     *                 created this class.
     */
    public static void createInstance(QuizActivity activity) {
        instance = new LoadingScreen();
        quizActivity = activity;

        instance.loadUiData();
    }

    /**
     * Return the instance of the Loading Screen.
     *
     * @return LoadingScreen's singleton instance
     */
    public static LoadingScreen getInstance() {
        return instance;
    }

    /**
     * Load the UI elements into memory from the QuizActivity instance.
     */
    public void loadUiData() {
        progressBar = (ProgressBar) quizActivity.findViewById(R.id.quiz_activity_progress);
        debugTextView = (TextView) quizActivity.findViewById(R.id.quiz_activity_debug_info);
        debugTextScroll = (ScrollView)
                quizActivity.findViewById(R.id.quiz_activity_debug_info_scroll);
        countDown = (TextView) quizActivity.findViewById(R.id.quiz_activity_loading_countdown);
    }

    /**
     * Show or hide debug info text view which contains debugging
     * data and the progress of loading.
     *
     * @param show Show the display if true, otherwise don't.
     */
    public void displayDebugInfo(Boolean show) {
        if (show) {
            addDebugInfo("Showing debug information screen");
            progressBar.setVisibility(View.GONE);
            debugTextScroll.setVisibility(View.VISIBLE);
        } else {
            addDebugInfo("Hiding debug information screen");
            progressBar.setVisibility(View.VISIBLE);
            debugTextScroll.setVisibility(View.GONE);
        }
    }

    /**
     * Add debug information to the screen. New line is automatically added.
     *
     * @param text Text to add in the end.
     */
    public void addDebugInfo(final String text) {
        logDebug("Loading Screen", text);
        debugText += "\n" + text;

        debugTextScroll.post(new Runnable() {
            @Override
            public void run() {
                debugTextView.setText(debugText);
                debugTextScroll.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    /**
     * Start the countdown timer with a target time in the future.
     *
     * @param millisInFuture The amount of time which the timer should be run.
     */
    public void createCountdownTimer(final long millisInFuture) {

        if (millisInFuture < 0) {
            String startingQuiz = "Starting Quiz";
            setCountDownDisplay(startingQuiz);
            return;
        }

        quizActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                countDownTimer = new CountDownTimer(millisInFuture, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        int remainingTime = (int) (millisUntilFinished / 1000);
                        int seconds = remainingTime % 60;
                        int minutes = remainingTime / 60;
                        int hours = minutes / 60;
                        minutes %= 60;

                        String displayText =
                                String.format(Locale.ENGLISH,
                                        "%02d:%02d:%02d", hours, minutes,seconds);
                        setCountDownDisplay(displayText);
                    }

                    @Override
                    public void onFinish() {
                        String startingQuiz = "Starting Quiz";
                        setCountDownDisplay(startingQuiz);
                        QuizLoader.getInstance().scheduleStartQuizInterface();
                    }
                };
                countDownTimer.start();
            }
        });
    }

    /**
     * Set the countdown display value on the screen.
     *
     * @param displayText The text to be displayed
     */
    public void setCountDownDisplay(final String displayText) {
        countDown.post(new Runnable() {
            @Override
            public void run() {
                countDown.setText(displayText);
            }
        });
    }

    /**
     * Hide the loading screen widgets and show the quiz ui.
     */
    public void hideLoadingScreen() {
        quizActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View loadingScreen = quizActivity.findViewById(R.id.loading_screen);
                loadingScreen.setVisibility(View.GONE);
                View quizUi = quizActivity.findViewById(R.id.quiz_ui);
                quizUi.setVisibility(View.VISIBLE);
                View bottomNav = quizActivity.findViewById(R.id.bottom_navigation);
                bottomNav.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Show the loading screen.
     */
    public void showLoadingScreen() {
        quizActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View loadingScreen = quizActivity.findViewById(R.id.loading_screen);
                loadingScreen.setVisibility(View.VISIBLE);
                View quizUi = quizActivity.findViewById(R.id.quiz_ui);
                quizUi.setVisibility(View.GONE);
                View bottomNav = quizActivity.findViewById(R.id.bottom_navigation);
                bottomNav.setVisibility(View.GONE);
                QuizFab.instance.hideFab();
            }
        });
    }
}
