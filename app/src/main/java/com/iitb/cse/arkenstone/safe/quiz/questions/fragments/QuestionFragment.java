package com.iitb.cse.arkenstone.safe.quiz.questions.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.Space;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iitb.cse.arkenstone.safe.R;
import com.iitb.cse.arkenstone.safe.quiz.QuizActivity;
import com.iitb.cse.arkenstone.safe.quiz.QuizFab;
import com.iitb.cse.arkenstone.safe.quiz.questions.data.Question;
import com.iitb.cse.arkenstone.safe.utils.AppData;

import java.util.Locale;

/**
 * The base question class fragment which is the root class for
 * all the other types of questions.
 */
public abstract class QuestionFragment extends Fragment {

    /**
     * The question for which this fragment is used.
     */
    protected Question question;

    /**
     * Generate the view for the child object. This view is
     * appended to the Question's view.
     */
    public abstract void generateChildView(View inflatedLayout);

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_question, container, false);
        generateQuestionView(view);
        generateChildView(view);

        // Add empty space at the bottom so that we scroll beyond the FAB
        Space empty = new Space(getActivity());
//        empty.setMinimumHeight(QuizFab.instance.getHeightFromBottom());

        LinearLayout questionFrag = (LinearLayout) view.findViewById(R.id.frag_question);
        questionFrag.addView(empty);

        return view;
    }

    private void generateQuestionView(View inflatedLayout) {

        TextView marksView = (TextView) inflatedLayout.findViewById(R.id.marks);
        marksView.setText(String.format(Locale.ENGLISH,
                getString(R.string.marks_display), question.maxMarks));
        if (question.marksObtained != null) {
            marksView.setText(String.format(Locale.ENGLISH,
                    getString(R.string.marks_display), question.marksObtained.toString() + "/" + question.maxMarks.toString()));
        }

        LinearLayout questionFrag = (LinearLayout) inflatedLayout.findViewById(R.id.frag_question);

        WebView questionView = AppData
                .getWebViewCache()
                .get("Question " + question.questionNumber + ". " + question.description);
        if (questionView.getParent() != null) {
            ((ViewGroup) questionView.getParent()).removeView(questionView);
        }

        if (!"".equals(question.module.description)) {
            WebView moduleView = AppData
                    .getWebViewCache()
                    .get("<span hidden>" + question.id + "</span>" + question.module.description);
            if (moduleView.getParent() != null) {
                ((ViewGroup) moduleView.getParent()).removeView(moduleView);
            }
            questionFrag.addView(moduleView);
        }
        questionFrag.addView(questionView);
    }

    /**
     * Set the question which this fragment displays.
     *
     * @param question The question to associate with the fragment.
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     * Setup the FAB for the question.
     */
    public abstract void setupFab();

    /**
     * Freeze a question in case submission fails.
     */
    public abstract void freezeQuestion();
}
